<?php
session_start();
include_once 'login_checker.php';
if(has_capabilities($uid, 'Attendance')==false){
	header('Location:home.php');
	exit();
}

include 'header.php';

include('weeks.php');



?>
<?php $cweek = curweek($db); // see above, this is the current week ?>
<script>
function search_attendance_for_student(){
	var from = document.getElementById('week_from').value;
	var to = document.getElementById('week_to').value;
	var student =  document.getElementById('student_id').value;
	
	if (student.length<=0 || student.length>9 ) {
		document.getElementById('submit_status').innerHTML="Please enter valid student ID";
		return false; 
	}
	
	if(parseInt(from)<=parseInt(to) && student!=""){
		var url = "search_attendance_for_student.php?fromweek="+from+"&toweek="+to+'&student_number='+student;
		$('#show_student_attendance').html('Loading... Please wait...').load(url)
	}
	else{
		document.getElementById('submit_status').innerHTML="Please select validate week range";
	}
}


$(window).load(function() {
if(window.location.hash !=""){
var hashval = window.location.hash.replace('#', '');
$('#student_id').val(hashval);
$('#week_to').val(<?php echo $cweek;?>);
search_attendance_for_student();
}
});

</script>
<div id="wrapper">
    <div id="wrapper_content">
        <h1 class="page_title">Check attendance for a student</h1>
<br />

<table width="100%" cellpadding="5" cellspacing="0">
         	<tr>
              <td colspan="2"><strong>Student ID :</strong> <input type="text" id="student_id" name="student_id" placeholder="Student ID" onfocus="document.getElementById('submit_status').innerHTML=''"/></td>
            </tr>
            <tr>
    <td colspan="2"><strong>Week From</strong>
    <select id="week_from" name="week_from" onfocus="document.getElementById('submit_status').innerHTML=''">
     <?php
		for($i=1;$i<=24;$i++){
			?>
            <option value="<?php echo $i;?>" <?php if($from==$i) echo 'selected="selected"';?>><?php echo $i;?></option>
            <?php
		}
		?>
    </select>
    &nbsp;&nbsp;
    <strong>To</strong> &nbsp;
    <select id="week_to" name="week_to" onfocus="document.getElementById('submit_status').innerHTML=''">
     <?php
		for($i=1;$i<=24;$i++){
			?>
            <option value="<?php echo $i;?>" <?php if($to==$i) echo 'selected="selected"';?>><?php echo $i;?></option>
            <?php
		}
		?>
    </select></td>
    </tr>
    <tr>
        <td colspan="2"><a href="javascript:;" class="small themebutton button" onClick="search_attendance_for_student()"><span>Search</span></a>&nbsp;&nbsp;
        <b id="submit_status" style="color:#F00;">&nbsp;</b>
        </td>
 	 </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
        </table>
		<br />	
        <div id="show_student_attendance"></div>
        </div>
    </div>

</div>
<?php
include 'footer.php';
?>
