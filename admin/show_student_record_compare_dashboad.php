<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');

$color_array = array("","#FF0000", "#f47901", "#108b03","#6e5134");
$today = date("Y-m-d");
$upper_limit = 0;


if(!isset($_GET['inactive'])){
	$only_active = 	' AND `student_status` = 0';
}
else{
	$only_active = '';
}

$stud_obj = $db->query("SELECT * FROM `students` WHERE 1 $only_active");

$students = $stud_obj->rows;

		if($_GET['levels']!=""){
			$levels = $_GET['levels'];
			$add_filter_levels = " AND level_id IN ($levels) ";
			$flag = 1;
			
			$levels_obj = $db->query("select * from `levels` where level_id IN ($levels)");
			$levels = $levels_obj->rows;
			$levels_txt = "";
			foreach($levels as $level){
				
				if($levels_txt=="")
					$levels_txt= $level['level'];
				else
					$levels_txt.=', '.$level['level'];
			}
			
			$filter_levels = $levels_txt;
		}
		else{
			$add_filter_topics = "";
			$filter_levels = 'All';
		}
		
		if($_GET['topics']!=""){
			$topics = $_GET['topics'];
			$add_filter_topics = " AND topic_id IN ($topics) ";
			$flag = 1;
			
			$topics_obj = $db->query("select * from `topics` where topic_id IN ($topics)");
			$topics = $topics_obj->rows;
			$topics_txt = "";
			foreach($topics as $topic){
				if($topics_txt=="")
					$topics_txt= $topic['topic'];
				else
					$topics_txt.=', '.$topic['topic'];
				
			}
			
			$filter_topics = $topics_txt;
		}
		else{
			$add_filter_topics = "";
			$filter_topics = 'All';
		}
		
		if($_GET['from_date']!=""){
			$from_date = date_mysql($_GET['from_date']);
			$add_filter_from =" AND expected_completion_date >= '$from_date' ";
			$flag = 1;

		}
		else{
			$add_filter_from = "";	
		}
		
		if(isset($_GET['from_date']) && $_GET['from_date']!=""){
			$filter_date = 'Between '.$_GET['from_date'] . ' and '.$_GET['to_date'];
		}
		else{
			$filter_date =  'Any';
		}
		
		if($_GET['to_date']!=""){
			$to_date = date_mysql($_GET['to_date']);
			$add_filter_to =" AND expected_completion_date <= '$to_date' ";
			$flag = 1;

		}
		else{
			$add_filter_to = "";
		}
		
		if(isset($_GET['observed_from']) && $_GET['observed_from']!=""){
			$filter_range = 'Between '.$_GET['observed_from'] . ' and '.$_GET['observed_to'];
		}
		else{
			$filter_range = 'Any';
		}
$values = array();
$labels = array();
$colors = array();

	$net_total_obj = $db->query("SELECT COUNT(*) AS net_total FROM `sobs` WHERE 1  $add_filter_levels $add_filter_topics $add_filter_from $add_filter_to and expected_completion_date<'$today'");
	$net_total = $net_total_obj->row;
	$ideal_mark = $net_total['net_total'];
	
	array_push($values,$ideal_mark);
	array_push($labels," ");
	array_push($colors,"#000000");

	
	if(isset($_GET['observed_to']) && $_GET['observed_to']!=""){
	$max_y_value = ceil($_GET['observed_to'] * 1.2);
	}
	else{
	$max_no_of_sobs_obj = $db->query("SELECT COUNT(*) AS max_no_of_sobs FROM `sobs` WHERE 1  $add_filter_levels $add_filter_topics $add_filter_from $add_filter_to ");
	$max_no_of_sobs_total = $max_no_of_sobs_obj->row;
	$max_y_value = $max_no_of_sobs_total['max_no_of_sobs'];
	}
	
	
	
	
foreach($students as $student){
	
	$stud_id = $student['student_id'];
	$sql = " AND `sob_id` IN (SELECT sob_id FROM `sobs` WHERE 1 $add_filter_levels $add_filter_topics $add_filter_from $add_filter_to)";
		
	$filter_query = "SELECT COUNT(*) AS user_finished_total FROM `sob_observations` WHERE `student_id` = '$stud_id'";

	if(isset($_GET['from_week']) && $_GET['from_week']!=""){
	  // observed_on
	  $filter_query .= " AND observed_on >= '". date_mysql($_GET['from_week'])."' and observed_on <= '". date_mysql($_GET['to_week'])."'";
	}

	if($flag==1){
	  $filter_query = $filter_query.$sql;
	}

	$user_finished_obj = $db->query($filter_query);
	$user_finished = $user_finished_obj->row;
	
	
	$user_finish = $user_finished['user_finished_total'];
	
	if(isset($_GET['observed_from']) && $_GET['observed_from']!=""){
		if($user_finish>=$_GET['observed_from'] && $user_finish<=$_GET['observed_to']){
			array_push($values,$user_finish);
			array_push($labels," ");
			array_push($colors,"#EECA06");
			
			
		}
	}
	else{
			array_push($values,$user_finish);
			array_push($labels," ");
			array_push($colors,"#EECA06");
	}
				
}

array_multisort($values, $labels, $colors);

$vals = "";
foreach($values as $value){
	if($vals==""){
		$vals = $value;
	}
	else{
		$vals.=",".$value;
	}
}

$labs = "";
foreach($labels as $label){
	if($labs==""){
		$labs = '" "';
	}
	else{
		$labs.=",".'" "';
	}
}


$cols = "";
foreach($colors as $color){
	if($cols==""){
		$cols = '"'.$color.'"';
	}
	else{
		$cols.=",".'"'.$color.'"';
	}
}

$applied_filter = "<strong>Levels</strong> : $filter_levels | <strong>Topics</strong> : $filter_topics <br /><strong> Expected Completion Date</strong> : $filter_date | <strong>No of Observed SOBs</strong> : $filter_range";

$json = '{
			"values" : ['.$vals.'],
			"labels" : ['.$labs.'],
			"colors" : ['.$cols.'],
			"applied_filter"  : "'.$applied_filter.'",
			"max_y_value"  : "'.$max_y_value.'"
		}';
echo $json;	
?>
