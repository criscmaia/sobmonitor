<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('includes/excel_reader.php');
include_once('login_checker.php');

function array_push_assoc($array, $key, $value)
{
		$array[$key] = $value;
		return $array;
}

	
	
	
		if($_FILES["student_upload"]["type"] == "application/vnd.ms-excel" || $_FILES["student_upload"]["type"] == "text/xls")
		{
			
			if ($_FILES["student_upload"]["error"] > 0)
			{
				$description="Invalid file uploaded by the user. Error : ".$_FILES["student_upload"]["error"];
				"Return code: " . $_FILES["student_upload"]["error"] . "<br />";
			}
			else
			{
				if (file_exists("students_list_temp/" . $_FILES["student_upload"]["name"]))
				{	
					unlink("students_list_temp/" . $_FILES["student_upload"]["name"]);
				}
				
				$excel_file_name = date('d_m_y_h_i_s')."_base".".xls";
				move_uploaded_file($_FILES["student_upload"]["tmp_name"],"students_list_temp/" .$excel_file_name);
				$description= $_FILES["student_upload"]["name"].' saved as '.$excel_file_name." and  uploaded";

				$file_path= "students_list_temp/" . $cur_file_name;
				
			}
	  }
	  else
	  {
		  $description="Invalid file uploaded by the user";
	
		  ?>
		  <script>alert('Invalid file. Please upload a file in xls format.');</script>
		  <?php
	  }

	
	$data = new Spreadsheet_Excel_Reader("students_list_temp/" . $excel_file_name, false, 'UTF-8');
	
	
	$count = $data->rowcount($sheet_index=0);
	$col_count = $data->colcount($sheet_index=0);
	
	$result_array = array();// used to store the index of  COLHEADS 	

	$template_array =array("Student Number", "First Name", "Last Name", "Email", "Network Name / User ID", "Visa", "Foundation");
	
	$trow_in_excel = $count-1;
		
		for ($i = 1; $i <= $col_count; $i++)
		{
			$result =  $data->val(1,$data->colindexes[$i]);
			
			for($j=0;$j<count($template_array);$j++)
			{
				
				if(strpos($result,$template_array[$j]) !== false)
				{
					array_push($result_array,$i);
				}
			}
		}
		$ignored_row = 0;
		$valid_count = 0;
		$updated_row = 0;
		$error_count = 0;
		
		for($excel_index=2;$excel_index<=$count;$excel_index++)
		{
			
				 
			for($templete_index=0;$templete_index<count($result_array);$templete_index++)
			{
				$c_head = $templete_index;
				$result= $data->val($excel_index,$data->colindexes[$result_array[$templete_index]]);
				
				$excel_array = array_push_assoc($excel_array, $c_head, $result);
			} 
			
			$flag = 0;
			$error = 0;
			
			
			if($excel_array[0]=='')
			{
				$ignored_row++;
			}
			else
			{	 
					$student_number = $db->escape($excel_array[0]);
					$firstname = $db->escape($excel_array[1]);
					$lastname = $db->escape($excel_array[2]);
					$email = $db->escape($excel_array[3]);
					$network_name = $db->escape($excel_array[4]);

				    $visa_status = $db->escape($excel_array[5]);
					if ( ($visa_status != 1) && ($visa_status != "Yes") ) {
						$visa_status = 0;
					} else {
						$visa_status = 1;
					}

					$foundation_status = $db->escape($excel_array[6]);
					if ( ($foundation_status != 1) && ($foundation_status != "Yes") ) {
						$foundation_status = 0;
					} else {
						$foundation_status = 1;
					}
					
					$chk_obj = $db->query("SELECT * FROM `students` WHERE `student_number` = '$student_number'");
					$chk_row = $chk_obj->num_rows;
					
					if($chk_row==0){
						$db->query("INSERT INTO `students` (`network_name`, `firstname`, `lastname`, `email`, `student_number`, `student_status`, `visa`, `foundation`) VALUES ('$network_name', '$firstname', '$lastname', '$email', '$student_number', '0', $visa_status, $foundation_status)");
						$student_id = $db->getLastId();
						$enc_id = md5($student_id);
						$db->query("UPDATE `students` SET `enc_id` = '$enc_id' WHERE `student_id` = '$student_id'");
						$valid_count++;
						
					}
					else{
						$student_obj = $chk_obj->row;
						$student_id = $student_obj['student_id'];
						$db->query("UPDATE `students` SET `network_name` = '$network_name', `firstname` = '$firstname', `lastname` = '$lastname', `email` = '$email', `visa`=$visa_status, `foundation`=$foundation_status WHERE `student_id` = '$student_id'");
						
						$updated_row++;
					}
					
			}
			
		}
			
	
?>
<script>
window.top.window.document.getElementById('base_d').innerHTML='<div><span style="color:green">Total new records : <strong><?php echo $valid_count?></strong></span> | <span style="color:green">Total update records : <strong><?php echo $updated_row?></strong></span> | <span style="color:red">Total Errors : <strong><?php echo $error_count;?></strong></span> | <span style="color:orange;">Total ignored rows : <strong><?php echo $ignored_row;?></strong></span> </div>';
window.top.window.$('#submit_button').text('Submit');
</script>
