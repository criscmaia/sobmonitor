<?php
session_start();
include 'login_checker.php';
include 'header.php';
?>

<link class="include" rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
<script src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
<script src="js/Chart_overall.js"></script>
<script>
$(document).bind('click', function(e) {
	var $clicked = $(e.target);
	if (!$clicked.parents().hasClass("filter_wrapper") && !$clicked.hasClass("ui-state-default") && !$clicked.hasClass("ui-icon") && !$clicked.hasClass("ui-datepicker-month") && !$clicked.hasClass("ui-datepicker-title") && !$clicked.hasClass("ui-datepicker-div") && !$clicked.hasClass("ui-datepicker") && !$clicked.hasClass("ui-datepicker-header") && !$clicked.parent().parent().parent().parent().hasClass("ui-datepicker-calendar") && !$clicked.parent().parent().parent().parent().hasClass("ui-datepicker")){
		$('.filter_holder').css('display','none');
	}
});
function open_filter(level){
	if($('#filter_holder_'+level).css('display')=='block'){
		$('#filter_holder_'+level).css('display','none');
	}
	else{
		$('#filter_holder_'+level).css('display','block');
		$('.datepicker').datepicker();
	}
}

function filter_graph_overall(type){
	var flag = 0;
	
	var levels = "";var topics = "";var sdate = ""; var edate = "";
	
	var level_arr = document.getElementsByName('filter_levels');
	for(var i=0;i<level_arr.length;i++){
		if(level_arr[i].checked==true){
			if(levels=="")
			levels = level_arr[i].value;
			else
			levels+=","+level_arr[i].value;
			
			flag=1;
			
		}
	}
	
	
	var arr = document.getElementsByName('filter_topics');
	for(var i=0;i<arr.length;i++){
		if(arr[i].checked==true){
			if(topics=="")
			topics = arr[i].value;
			else
			topics+=","+arr[i].value;
			
			flag = 1;
		}
	}
	
	if(document.getElementById('start_date_from').value!=""){
		flag=1;
		sdate = document.getElementById('start_date_from').value;
		
		if(document.getElementById('start_date_to').value==""){
			$('#date_filter_status').html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
		
	}
	
	if(document.getElementById('start_date_to').value!=""){
		flag=1;
		edate = document.getElementById('start_date_to').value;
		
		if(document.getElementById('start_date_from').value==""){
			$('#date_filter_status').html("Please fill both 'from' & 'to' to apply this filter");
			return;
		}
	}
	
	if(document.getElementById('include_inactive').checked==true){
		include = '&inactive=1';
		flag=1;
	}
	else{
		include = '';
	}
	
	if(flag==1){
		var filters = '&levels='+levels+'&topics='+topics+'&from_date='+sdate+'&to_date='+edate+include;
	}
	else{
		var filters ="";
	}
	
	
	
		var stud = document.getElementById('student_id').value;
		var url = 'show_student_record_compare.php?student_id='+stud+filters;
		$.getJSON(url, function(data){
			open_filter(type);
			generate_graph(data.values, data.labels, data.colors, 'chart1', data.applied_filter,data.max_y_value)
		});
		
}

function generate_graph(values, labels, colors, holder, applied_filter,max_y_value){
	$.jqplot.config.enablePlugins = true;
			var s1 = values;
			var ticks = labels;
			
			plot1 = $.jqplot(holder, [s1], {
				// Only animate if we're not using excanvas (not in IE 7 or IE 8)..
				animate: !$.jqplot.use_excanvas,
				seriesColors:colors,
				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					shadow : false,
					rendererOptions: {
					// Set varyBarColor to tru to use the custom colors on the bars.
					varyBarColor: true
				},
					pointLabels: { show: false }
				},
				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						ticks: ticks,
						label : 'Progress of students + Expected progress (by <?php echo date('d.m.Y');?>)',
						labelOptions: {
							fontFamily: 'Arial',
							fontSize: '12px'
						  }
					},
					yaxis:{
						tickOptions:{
						formatString: "%d",
						fontSize: '12px'
						},
					   label:'Number of SOBs',
					   min:0,
  				    	max: max_y_value,
					   labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					   labelOptions: {
							fontFamily: 'Arial',
							fontSize: '12px'
						  }
					}
				},
				highlighter: { show: true }
			});
			plot1.replot();
			$('#graph_filter_info').html(applied_filter);
}

function check_student_id_valid(){
	if(document.getElementById('student_id').value!=""){
		var stud = document.getElementById('student_id').value;
		$('#student_details').html('Please wait... Loading...').load('check_student_id_valid.php?student_id='+stud,function(){
			var status = $('#process_status').val();
			if(status=="1"){
				show_student_record_compare();
			}
			else{
				$('#content').css('display','none');
			}
		});
	}
}

function show_student_record_compare(){
	if(document.getElementById('student_id').value!=""){
		var stud = document.getElementById('student_id').value;
		
		$('#content').css('display','')

		var url = 'show_student_record_compare.php?student_id='+stud;
		$.getJSON(url, function(data){
			generate_graph(data.values, data.labels, data.colors, 'chart1', data.applied_filter,data.max_y_value)
			$('.display_student_name').html(data.student)
		});
		
		
		var url = 'show_student_record_overall.php?student_id='+stud;
		$.getJSON(url, function(data){
								
			var radarChartData = data;
			var myRadar = new Chart(document.getElementById("canvas2").getContext("2d")).Radar(radarChartData,{scaleShowLabels : false, pointLabelFontSize : 10});
			
		});
		

	}
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode
	
	if (charCode ==13)
	check_student_id_valid();
	
	return true;
}


</script>
<div id="wrapper">
    <div id="wrapper_content">
    	<h1 class="page_title">Comparative Report - All levels</h1>
         <table width="100%" cellpadding="0" cellspacing="0">
         	<tr>
              <td colspan="2"><strong>Student ID :</strong> <input type="text" id="student_id" name="student_id" placeholder="Student ID"  onkeypress="return isNumberKey(event)"/> &nbsp;&nbsp; <a class="small themebutton button" href="javascript:;" onClick="check_student_id_valid()">Submit</a> </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
        </table>
        <br>
        <div id="student_details"></div>
		<br>
        <div id="content" style="display:none; position:relative;">
        
        <?php $legend_graph = '<div id="graph_legend">
                <ul>
                    <li><div class="legend_key other_students"></div>Other students</li>
                    <li><div class="legend_key student_selected"></div><div class="display_student_name"></div></li>
                    <li><div class="legend_key ideal"></div>Expected by '.date('d.m.Y').'</li>
                </ul>
            </div>'; 
			
			$legend_graph1 = '<div id="graph_legend">
                <ul>
                    <li><div class="legend_key other_students"></div>Other students</li>
					<li><div class="legend_key student_selected"></div><div class="display_student_name"></div></li>
                </ul>
            </div>'; 
			
			$filter = '<div class="filter_wrapper"><b onClick="open_filter(1)" class="highlight_color">Filter</b> 
				<div class="filter_holder highlight_color" id="filter_holder_1">
				<strong>Levels</strong><a href="javascript:;" class="close_button" onClick="open_filter(1)" style="float:right;">X</a><br />
        				<input type="checkbox" value="1" name="filter_levels" id="level"> Threshold
						<input type="checkbox" value="2" name="filter_levels" id="level"> Typical
						<input type="checkbox" value="3" name="filter_levels" id="level"> Excellent <br>
				<strong>Topics</strong><br /><ul class="filter_list">';
				
		$topics_obj = $db->query("select * from `topics` where 1");
		$topics = $topics_obj->rows;
		
		foreach($topics as $topic){
			$filter.='<li><input type="checkbox" value="'.$topic['topic_id'].'" name="filter_topics" id="topic"> ' . $topic['topic'] . '</li>';
			
		}
					
							
					$filter.='</ul><div class="clear"></div>
					<strong>Expected Completion Date</strong><br />
                <input type="text" style="width:80px;" placeholder="From" class="datepicker" name="start_date_from" id="start_date_from" onFocus="document.getElementById(\'date_filter_status\').innerHTML=\'\'">&nbsp;&nbsp;
                <input type="text" style="width:80px;" placeholder="To" class="datepicker" name="start_date_to" id="start_date_to" onFocus="document.getElementById(\'date_filter_status\').innerHTML=\'\'"><br /><span id="date_filter_status" class="error"></span><br />
				<input type="checkbox" id="include_inactive" name="include_inactive" value="1" /> Include inactive students<br />
				<a class="small themebutton button" href="javascript:;" onClick="filter_graph_overall(\'1\')">Submit</a><br />
				
				</div>
			</div>';

		
			
			?>
            
            <div class="canvas_holder">
            
            	<div class="graph_display_details"><h2>Overall progress vs <span class="display_student_name"></span>'s progress <?php echo $filter;?></h2>
                    <div class="graph_filter_info" id="graph_filter_info"><strong>Levels</strong> : All | <strong>Topics</strong> : All <br /><strong> Expected Completion Date</strong> : Any | <strong>No of Observed SOBs</strong> : Any</div>
                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
            
                <div id="chart1" style=" width:100%; height:450px;"></div>
            </div>
            
    		
			<div class="canvas_holder"><div><h2>Overall progress vs <span class="display_student_name"></span>'s progress  <?php //echo $filter_2;?></h2><?php echo $legend_graph1;?></div><canvas id="canvas2" height="450" width="600"></canvas></div>
        </div>
        
    </div>
    	
</div>



<?php
include 'footer.php';
?>