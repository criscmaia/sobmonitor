<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
extract($_GET);
//sob_observations

$sob_obj = $db->query("SELECT * FROM `sobs` WHERE `sob_id` = '$sob_id'");
$sob_details = $sob_obj->row;
?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grayout_panel">
 <tr>
    <th height="26" colspan="2">VIEW SOB DISCUSSION</th>
  </tr>
</table>
<div style="max-height:<?php $height-200;?>px; overflow:auto; padding:10px;">
<h4><?php echo $sob_details['sob'];?></h4>
<p><?php echo $sob_details['sob_notes'];?></p>

<table width="100%" border="0" cellspacing="0" cellpadding="5" class="dues_table" id="reply_table">
    <tr class="dues_header_tr">
        <th width="120">Said by</th>
        <th>Notes</th>
        <th width="100">Datetime</th>
    </tr>
    <?php
    
	$thread_obj = $db->query("SELECT * FROM `sob_discussion` WHERE `sob_id` = '$sob_id'");
	$reply_no = $thread_obj->num_rows; 
	if($reply_no>0){
		$replies = $thread_obj->rows;
		$r=0;
		foreach($replies as $reply){
			$r++;
			
			if($reply['user_type']=="1"){
				 $staff_id = $reply['user_id'];
				 $staff_obj = $db->query("SELECT * FROM `staffs` WHERE `staff_id` = '$staff_id'");
				 $staff_detail = $staff_obj->row;
				 $staff_name = $staff_detail['firstname'] . ' ' . $staff_detail['lastname'];
			}
			
			?>
			<tr>
				<td><?php echo ($reply['user_type']=="1")? $staff_name:'Student';?></td>
				<td><?php echo $reply['discussion_note'];?></td>
				<td><?php echo date('d.m.Y',strtotime($reply['discussion_datetime']));?></td>
			</tr>
			<?php
		}
	}
	else{
		echo '<tr><td colspan="3">No reply found</td></tr>';
	}
    ?>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
    <td>
    <form name="reply_form" id="reply_form" enctype="multipart/form-data" method="POST" target="hidden_iframe">
    <input name="sob_id" id="sob_id" type="hidden" value="<?php echo $sob_id;?>">
    <textarea id="discussion_note" name="discussion_note" placeholder="Note" style="height:50px; width:98%;"></textarea>
    </form>
    </td>
    <td width="100"><a class="small themebutton button" style="float:right;" onClick="insert_discussion_note()" href="javascript:;">Submit</a></td>
    </tr>
</table>
</div>