<?php
session_start();
include_once('login_checker.php');
extract($_GET);


	$today = date('Y-m-d');
	$week = date('Y-m-d',strtotime("+7 days"));
	
	
	?>
    <br />
<br />
<br />


			
	<?php
$query="select s.sob_id, s.sob, s.url, l.level, t.topic, s.expected_completion_date , s.expected_start_date from `sobs` s, `levels` l, `topics` t where s.level_id = l.level_id and s.topic_id = t.topic_id ";

if($start_date_from !='' && $start_date_to =='' )
	{
		$start_date_from=date_mysql($start_date_from);
		$query.=" and s.expected_start_date >= '$start_date_from' ";
	}
if($start_date_from =='' && $start_date_to!='')
{
	$start_date_to=date_mysql($start_date_to);
	$query.=" and s.expected_start_date <= '$start_date_to '";
}
if($start_date_from!="" && $start_date_to!="")
{
	$start_date_from=date_mysql($start_date_from);
	$start_date_to=date_mysql($start_date_to);
	$query.="and s.expected_start_date BETWEEN '$start_date_from' and '$start_date_to' ";	
}
if($completed_date_from!='' && $completed_date_to=='' )
{
		$completed_date_from=date_mysql($completed_date_from);
		$query.=" and s.expected_completion_date >= '$completed_date_from' ";
}
if($completed_date_to!='' && $completed_date_from=='' )
{
	$completed_date_to=date_mysql($completed_date_to);
	$query.=" and s.expected_completion_date <= '$completed_date_to'";
}
if($completed_date_from!="" && $completed_date_to!="")
{
	$completed_date_to=date_mysql($completed_date_to);
	$completed_date_from=date_mysql($completed_date_from);
	$query.=" and s.expected_completion_date BETWEEN '$completed_date_from' and '$completed_date_to' ";	
}
if($sob_filter!="")
{
	$query .= " and s.sob like '%$sob_filter%' ";
}
if(isset($filter_topics))
{
	 $filter_topics= implode(',',$filter_topics);
	 $query .= " and s.topic_id in ( $filter_topics ) ";
}
if(isset($filter_levels))
{
	 $filter_levels= implode(',',$filter_levels);
	 $query .= " and s.level_id in ( $filter_levels ) ";
}

if(isset($filter_keywords)){
	$keywords_sob_id = implode(',',$filter_keywords);
	
	if($keywords_sob_id!=""){
			$query.=" AND s.sob_id IN ($keywords_sob_id) ";
	}
}



	$query.=" ORDER BY s.level_id";



$sobs_obj = $db->query($query);
$sobs_no = $sobs_obj->num_rows;

	
	if($sobs_no!=0){
		$sobs = $sobs_obj->rows;
		
		$topic_name = "";
		$level_name = "";
		?>
        <table width="100%" cellpadding="0" cellspacing="0">
				<tr>
                  <td align="left"><strong>ECD</strong> - <strong>E</strong>xpected <strong>C</strong>ompletion <strong>D</strong>ate</td>
				  <td align="right">
                  <ul class="observe_legend">
                  	<li class="color_box sob_expired">&nbsp;</li>
                    <li>Overdue</li>
                    <li class="color_box sob_expire_today">&nbsp;</li>
                    <li>Active</li>
                  </ul>
                  </td>
				</tr>
	
			</table>
		<table width="100%" border="0" cellpadding="10" cellspacing="1">
		<?php
		$s=0;
		foreach($sobs as $sob){
		
		$sob_id = $sob['sob_id'];
		
		if($level_name!=$sob['level']){
			?>
			<tr>
				<td class="level_name" colspan="2"><?php echo $sob['level'];?></td>
			</tr>
			<?php
			$level_name=$sob['level'];
		}
		
		if($topic_name!=$sob['topic']){
			?>
			<tr>
				<td class="topic_name" colspan="2"><?php echo $sob['topic'];?></td>
			</tr>
			<?php
			$topic_name=$sob['topic'];
			$s=0;
		}
		
		$s++;
		
		$obs_obj = $db->query("SELECT DISTINCT student_id FROM `sob_observations` WHERE  `sob_id` = '$sob_id'");
		$obs_no = $obs_obj->num_rows;

		

		?>
		<tr class="sob_highlight">
			  <td align="left" <?php if($sob['expected_completion_date']<$today) echo 'class="sob_expired"'; else if($sob['expected_start_date']<$today && $sob['expected_completion_date']>$today && $obs_no==0) echo 'class="sob_expire_today"';?> width="10"><?php echo $sob_id;?></td>
			  <td align="left"><?php echo $sob['sob'];?></Td>
		</tr>
		<tr>      
			  <td colspan="2">
			  <div style="float:left;"><strong>ECD:</strong> <?php echo date_ft($sob['expected_completion_date']); if($sob['expected_completion_date']<$week && $obs_no==0 && $sob['expected_completion_date']>=$today) { echo ' <b>'. DayDifference($today, $sob['expected_completion_date']) . ' day(s) left</b>'; }?></div> 
              
              <div style="float:right;" id="sob_status_<?php echo $sob['sob_id'];?>">
              Observed : <span id="total_sobs_observed_<?php echo $sob['sob_id'];?>"><?php echo $obs_no;?></span>&nbsp;&nbsp;
              <a class="small themebutton button" href="javascript:;" onClick="observe_sob_multiple('<?php echo $sob_id;?>')">Observe - Multiple students</a>
              </div>
		
              
			  </td>
			</tr>	
		<?php
		}
		?>
	</table>
		<?php
	}
	else{
		?>
		<table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1" align="left">
		 <tr>
			<Td align="center"><b>-- No results found --</b></Td>
		  </tr>
		</table>
		<?php
	}
	?>
