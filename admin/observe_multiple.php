<?php
session_start();
include 'login_checker.php';
include 'header.php';


?>
<style>
#myDiv div{
	padding:10px;
	
}
</style>
<script type="text/javascript" language="JavaScript" src="js/sobs.js"></script>
<script>
$(document).ready(function() {
	$('.datepicker').datepicker();
	$("#filter_sob_keywords").autocomplete("keyword_suggestions.php");
});
function sorting_field_name(field)
{
	var curr_sort = $('#sorting_field_name').val();
	var sort_by = $('#sorting_by').val();

	if(curr_sort == field){
		if(sort_by=='ASC')
	$('#sorting_by').val('DESC');
	else
	$('#sorting_by').val('ASC');	
	}
	
	else{
	$('#sorting_field_name').val(field);
	$('#sorting_by').val('ASC');
	}
	search_sob();
	//filter_sob();
	
}
function filter_sob()
{

	
	var sorting_field_name = $('#sorting_field_name').val();
	var sorting_by = $('#sorting_by').val();
	
	$('#page_contents').load('list_sobs.php','sorting_field_name='+sorting_field_name+'&sorting_by='+sorting_by);

}

function observe_sob_multiple(sob){
	var height = $(window).height();
	var url = "observe_sob_multiple.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&sob_id='+sob);
}





function addElement() {
  document.getElementById('status_message_display').innerHTML = ""; 
  var tobeignored = 0;
  var status_message = "";
  var ni = document.getElementById('myDiv');
  var studid = document.getElementById('sid').value;
  studid = studid.toUpperCase();

  if ( studid.length == 0 ) { return false; }

  if ( studid.length>9 ) {
     alert(studid+': Student ID too long, it cannot contain more than 9 characters');
     return false;
  }

  var validsequence = /^[a-zA-Z0-9]*$/.test(studid);

  if ( !validsequence ) {
    alert(studid+' contains invalid characters. Only letter and numbers are allowed for student ID');
    return false;
  }

  if (students.indexOf(studid) == - 1) {
    var newdiv = document.createElement('div');
    var divIdName = 'Div'+studid;
    newdiv.setAttribute('id',divIdName);
    var studidAndName = studid;

    if (slist.hasOwnProperty(studid) ) {
      studidAndName = studid + " - " + slist[studid];
      studidAndName = studidAndName.substring(0,40);
	  students.push(studid);
	  newdiv.innerHTML = studidAndName + ' <a class="small themebutton button" style="float:right;" href=\'javascript:;\' onclick=\'removeElement("'+divIdName+'")\'>DEL</a>';
	  <!--status_message = "<font color='#008800'>Added</font>";-->
    }
	else  if (ulist.hasOwnProperty(studid)) {
      studidAndName = '<font color="#FB7834">'+studid+ " - " + ulist[studid]+'</font>';
	  newdiv.innerHTML = studidAndName + '<span style="float:right;">Already Observed - Ignored Now</span>';
	  tobeignored = 1;
	 <!-- status_message = "<font color='#FB7834'>Already Observed</font>";-->
	  
	 
    }else {
      studidAndName = '<font color="#FF0000">'+studid+'</font>';
	  newdiv.innerHTML = studidAndName + ' <a class="small themebutton button" style="float:right;" href=\'javascript:;\' onclick=\'removeElement("'+divIdName+'")\'>DEL</a>';
	  tobeignored = 1;
	 <!-- status_message = "<font color='#FF0000'>Already added</font>";-->
	   
    }
    
    
	 //ni.insertBefore(newdiv,myDiv.childNodes[0]);
	 if(IgnoredStudents.indexOf(studid) == -1){ 
		 if(tobeignored == 1){
			 IgnoredStudents.push(studid);
		 }
		 $(ni).prepend(newdiv);
		 document.getElementById('myDiv').style.display = "block";
		 document.getElementById('counter').innerHTML = parseInt(document.getElementById('counter').innerHTML) + 1;
		 
		 
	 }else{
		document.getElementById('status_message_display').innerHTML = 'Note : <strong>'+studid+'</strong> : Already added below!!'; 
	 }
	 
	 
   } else {
   alert(studid+' - already added');
   
   }
   document.getElementById('sid').value = "";
   return false;
}

function removeElement(divNum) {
  // Removing from array:
  var studid = divNum.substr(3);
  var idx = students.indexOf(studid); // Find the index
  if(idx!=-1) students.splice(idx, 1);

  // Removing from screen:
  var d = document.getElementById('myDiv');
  var toberemoved = document.getElementById(divNum);
  d.removeChild(toberemoved);
  document.getElementById('counter').innerHTML = parseInt(document.getElementById('counter').innerHTML)- 1;

  if (parseInt(document.getElementById('counter').innerHTML) == 0) {
    document.getElementById('myDiv').style.display = "none";
  }
}



function submitdata() {
	if(students.length>0){
		var ids = 0;
		
		for(var i=0;i<students.length;i++){
			ids+=','+students[i];
		}
		var sob = $('#sob_id').val();
		$('#observe_holder').html('Submitting... Please wait...')
		$.post('observe_sob_multiple_submit.php',{students_list:ids, sob_id:sob},function(response){
			$('#observe_holder').html('<br><div align="center">The above sob has been observed for <h1 style="margin: 0;font-size:50px;padding:0px;">'+response+'</h1>Students</div>');
			var current_status = parseInt($('#total_sobs_observed_'+sob).html())+ parseInt(response);
			$('#total_sobs_observed_'+sob).html(current_status);
			
			
		});
	}
	else{
		alert("Please add atleast one student to submit");
	}
}

</script>
<div id="wrapper">
    <div id="wrapper_content">
    <h1 class="page_title">Observe SOBs - Multiple Students</h1>
<br />
<br />

      <form id="sob_filter_form" name="sob_filter_form" >
        <input type="hidden" name="sorting_field_name" id="sorting_field_name" value="s.sob_id" />
        <input type="hidden" name="sorting_by" id="sorting_by" value="ASC" />
        <div class="sob_filter_holder highlight_color">
        <table width="100%" border="0" cellpadding="6" cellspacing="1">
        <tr>
        	<td class="sub_headings" align="left">Filter SOBs</td>
            <td align="right" width="100"><a href="javascript:;" class="close_button" onclick="toggle_graph('sob_filter',this)">- Hide</a></td>
        </tr>
        </table>
        <div  id="sob_filter">
        <table width="100%" border="0" cellpadding="6" cellspacing="1">
        <tr>
        	<td width="6%"><strong>Levels</strong></td>
            <td colspan="2"> <?php
					$levels_obj = $db->query("select * from `levels` where 1");
					$levels = $levels_obj->rows;
					$l=0;
					foreach($levels as $level){						
						?>
						<input type="checkbox" id="level_<?php echo $level['level_id'];?>" name="filter_levels[<?php echo $l++;?>]" value="<?php echo $level['level_id'];?>"> <?php echo $level['level'];?> 
						<?php
					}
		?></td>
         </tr>
         <tr>
            <td><strong>Topics</strong></td>
            <td colspan="2">
            
             <?php
				$topics_obj = $db->query("select * from `topics` where 1");
				$topics = $topics_obj->rows;
				$t=0;
				foreach($topics as $topic){
					?>
					<input type="checkbox" id="topic_<?php echo $topic['topic_id'];?>" name="filter_topics[<?php echo $t++;?>]" value="<?php echo $topic['topic_id'];?>"> <?php echo $topic['topic'];?> 
					<?php
				}
		?>
            </td>
          </tr>
          <tr>
            <td><strong>SOB</strong></td>
            <td colspan="2">
           <textarea id="sob_filter" name="sob_filter" placeholder="Enter part of the SOB" style="height:17px; width:98%;"></textarea>
            </td>
          </tr>
          <tr>
            <td><strong>Start Dates</strong></td>
            <td colspan="2">
                <input type="text" id="start_date_from" name="start_date_from" class="datepicker" placeholder="Start From" style="width:80px;" />&nbsp;&nbsp;
                <input type="text" id="start_date_to" name="start_date_to" class="datepicker" placeholder="Start To" style="width:80px;" />
            </td>
          </tr>
          
          <tr>
            <td><strong>Expected Completion Dates</strong></td>
            <td colspan="2">
                
                <input type="text" id="completed_date_from" name="completed_date_from" class="datepicker" placeholder="Expected From " style="width:90px;" />&nbsp;&nbsp;
                <input type="text" id="completed_date_to" name="completed_date_to" class="datepicker" placeholder="Expected To" style="width:80px;" />
            </td>
          </tr>
          
          <tr>
            <td style="border-bottom:none;"><strong>Keywords</strong></td>
            <td><input type="text" id="filter_sob_keywords" name="filter_sob_keywords"  onkeyup="add_keyword_filter(this,event)" style="width:496px;" /></td>
            <td width="150"></td>
          </tr>
          <tr>
            <td style="border-top:none; border-right:none"><span style="border-top:none"><a class="small themebutton button" onclick="search_sob_multiple_observe()" href="javascript:;">Filter</a></span></td>
            <td colspan="2" style="border-top:none">
            <input type="hidden" name="filter_keyword_count" id="filter_keyword_count" value="0">
               <ul id="search_sob_keywords_tags" style="list-style: none outside none; margin-left: 0; margin-top: -12px; padding: 0; width: 95%;">
               
               </ul></td>
        </tr>
        </table>
        </div>
        </div>
        <br />


        </form>


        <div id="page_contents">
            <?php
            //include 'list_sobs_observe.php';
            ?>
      </div>
      
  </div>
</div>
<?php
include 'footer.php';
?>