<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');
//DEFAULT SORTING ORDER


extract($_GET);



?>
<div id="sobs_filtered_list">
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25" align="left">S.No</th>
          <th width="120" align="left">Student Number</th>
          <th align="left">First Name <span class="sort_field" style="float:right;" id="sort_field_level"><a id="button_level" href="javascript:sorting_field_name('fname')"><img src="images/sort-neutral.png" title="Sort" /></a></span> </th>
          <th align="left">Last Name  <span class="sort_field" style="float:right;" id="sort_field_level"><a id="button_level" href="javascript:sorting_field_name('lname')"><img src="images/sort-neutral.png" title="Sort" /></a></span> </th>
          <th width="120" align="left">Email </th>
          <th width="20" align="left">Visa</th>
          <th width="20" align="left">Fnd.</th>
          <th width="100" align="left">Threshold <span class="sort_field" style="float:right;" id="sort_field_level"><a id="button_level" href="javascript:sorting_field_name('threshold')"><img src="images/sort-neutral.png" title="Sort" /></a></span></th>
          <th width="100" align="left">Typical <span class="sort_field" style="float:right;" id="sort_field_level"><a id="button_level" href="javascript:sorting_field_name('typical')"><img src="images/sort-neutral.png" title="Sort" /></a></span></th>
          <th width="100" align="left">Excellent <span class="sort_field" style="float:right;" id="sort_field_level"><a id="button_level" href="javascript:sorting_field_name('excellent')"><img src="images/sort-neutral.png" title="Sort" /></a></span></th>
          <th width="120" align="left">Tutor <span class="sort_field" style="float:right;" id="sort_field_level"><a id="button_level" href="javascript:sorting_field_name('staff_id')"><img src="images/sort-neutral.png" title="Sort" /></th>
          <?php
        	if(has_capabilities($uid,'Manage Students')==true){
            ?>
          <th width="120" align="left">Actions</th>
          
         <!-- <th width="60" align="left">Edit</th>-->
          <?php
		}
		?>
         
      </tr>
<?php
//$query="select * from `students` where 1 ORDER BY $sorting_field_name $sorting_by";

$query =  "
-- student has been observed for some sobs but some others are missing
SELECT students.student_id, students.staff_id, students.firstname as fname, students.lastname as lname,
       students.student_number , students.student_status, students.email, students.visa as visa, students.foundation as foundation,
       sum(sobs.level_id=1) as threshold,sum(sobs.level_id=2) as typical,sum(sobs.level_id=3) as excellent
   FROM students, sobs
   WHERE (sobs.sob_id not in
        (select sob_observations.sob_id from sob_observations where sob_observations.student_id = students.student_id))
     AND sobs.expected_completion_date < CURDATE()
   GROUP BY students.student_id
UNION
-- student has never been observed
SELECT students.student_id, students.staff_id, students.firstname as fname, students.lastname as lname, students.student_number,
       students.student_status, students.email, students.visa as visa, students.foundation as foundation,
       sum(sobs.level_id=1) as threshold,sum(sobs.level_id=2) as typical,sum(sobs.level_id=3) as excellent
   FROM students, sobs
   WHERE students.student_id not in
          (select distinct sob_observations.student_id from sob_observations)
      AND sobs.expected_completion_date < CURDATE()
   GROUP BY students.student_id
UNION
-- there are no sobs to be observed for this student (everything done)
SELECT students.student_id, students.staff_id, students.firstname as fname,
students.lastname as lname, students.student_number, students.student_status, students.email, students.visa as visa, students.foundation as foundation,
 0 as threshold, 0 as typical, 0 as excellent
   FROM students, sobs
   WHERE not exists
     (select sobs.sob_id from sobs WHERE sobs.expected_completion_date < CURDATE() AND
      sob_id not in (select sob_observations.sob_id from sob_observations where
       sob_observations.student_id = students.student_id) )
   GROUP BY students.student_id
ORDER BY $sorting_field_name $sorting_by";

$student_obj = $db->query($query);
$student_no = $student_obj->num_rows;

if($student_no!=0){
	$students = $student_obj->rows;
	$s=0;
	foreach($students as $student){
	$s++;
	
	$staff = $student['staff_id'];
	
	?>
    <tr <?php if($student['student_status']=="1") echo 'class="highlight_color"';?> id="student_row_<?php echo $student['student_id'];?>">
          <td align="left" valign="top"><?php echo $s;?></td>
          <td align="left" valign="top"><?php echo $student['student_number'];?></Td>
          <td valign="top" align="left"><?php echo $student['fname'];?></td> 
          <td valign="top" align="left"><?php echo $student['lname'];?></Td>
          <td align="left" valign="top"><?php echo $student['email'];?></Td>
          <td align="left" valign="top"><?php if ($student['visa']) { echo "Y";} else {echo " ";} ?></Td>
          <td align="left" valign="top"><?php if ($student['foundation']) { echo "Y";} else {echo " ";} ?></Td>
          <td>
		     <a href="observation.php#<?php echo $student['student_number'];?>-1" target="_blank" title="Observe Threshold SOBs"><?php echo $student['threshold'];?></a>&nbsp;&nbsp;&nbsp;&nbsp;
			<?php
            if($student['threshold']!="0"){
            	echo  '<img src="images/danger.png">';
            }
            else{
            	echo '<img src="images/tick.png">';
            }
            ?>
          </Td>
          <td><a href="observation.php#<?php echo $student['student_number'];?>-2" target="_blank" title="Observe Typical SOBs"><?php echo $student['typical'];?></a>&nbsp;&nbsp;&nbsp;&nbsp;
          <?php
            if($student['typical']!="0"){
            	echo  '<img src="images/danger.png">';
            }
            else{
            	echo '<img src="images/tick.png">';
            }
            ?>
          </Td>
          <td><a href="observation.php#<?php echo $student['student_number'];?>-3" target="_blank" title="Observe Excellent SOBs"><?php echo $student['excellent'];?></a>&nbsp;&nbsp;&nbsp;&nbsp;
          <?php
            if($student['excellent']!="0"){
            	echo  '<img src="images/danger.png">';
            }
            else{
            	echo '<img src="images/tick.png">';
            }
            ?>
          </Td>
          <td id="tutor_<?php echo $student['student_number'];?>">
          <?php
		  if($staff=="0"){
			  	?>
          		<a href="javascript:;" title="Students Tutor" onclick="student_tutor('<?php echo $student['student_number'];?>')">TUTOR</a>
          	   	<?php
		  }
		  else{
				 $tutor_obj = $db->query("select * from `staffs` where `staff_id` = '$staff'");
				 $tutor = $tutor_obj->row;
			 	?>
          		<a href="javascript:;" title="Students Tutor" onclick="student_tutor('<?php echo $student['student_number'];?>')"><?php echo $tutor['firstname'];?> <?php echo $tutor['lastname'];?></a>
          	   	<?php
		  }
		  ?>
          </td>
           <?php
        	if(has_capabilities($uid,'Manage Students')==true){
            ?>
          <td valign="top" class="process_td">
           
         
          
          <div class="action_holder">
          <a href="javascript:;"><div id="actions_<?php echo $student['student_id'];?>" class="actions_list">Select action</div></a> 
                 
                <div id="actions_process_div_<?php echo $student['student_id'];?>" class="actions_process_div">
                    <div>
                    	
						  <a href="check_attendance.php#<?php echo $student['student_number'];?>" target="_blank" title="Attendance">View attendance</a>
						  <a href="observation.php#<?php echo $student['student_number'];?>" target="_blank" title="Observe SOBs">Observe SOBs</a>
						  <a href="javascript:;" title="Students Report" onclick="student_report('<?php echo $student['student_number'];?>')">Student Report</a>
						  <a href="javascript:;" title="Students Notes" onclick="student_notes('<?php echo $student['student_number'];?>')">Notes</a>
                          
                          <span id="student_status_<?php echo $student['student_id'];?>"><?php 
						  if($student['student_status']=="1"){
							  echo '<a href="javascript:;" onclick="student_activate('. $student['student_id'] . ')" title="Activate Student">Activate Student</a>';
						  }
						  else if($student['student_status']=="0"){
							 echo '<a href="javascript:;" onclick="student_deactivate('. $student['student_id'] . ')" title="Deactivate Student">Deactivate Student</a>';
						  }
						  ?></span>
						  
                    </div>
                </div>
               </div>
                
          
          </Td>
          <?php /*?><td align="left" valign="top">
          <a href="javascript:;" title="Edit Student" onclick="edit_student('<?php // echo $student['student_id'];?>')"><img src="images/edit.png" border="0" alt="Edit Student" /></a> 
          </td><?php */?>
 
          <?php
		}
		?>
      </tr>
    <?php
	}
}
else{
	?>
     <tr>
        <Td align="center" colspan="8"><br /><b>-- No results found --</b></Td>
        </tr>
    <?php
}
?>
</table>
</div>
