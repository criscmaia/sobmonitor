<?php
session_start();
include_once 'login_checker.php';
include 'header.php';

  $nowcrn = $db->escape($_POST['sendnowcrn']);
  $mycrn = $db->escape($_POST['sendcrn']);
 
  $week = $db->escape($_POST['sendweek']);
  $rawdata = $db->escape($_POST['rawdata']);
 
 
  if ( $nowcrn != "other") {
    echo "Using CRN $nowcrn<br>";
    $crn = $nowcrn;
  }

  if ( $mycrn != "other") {
    echo "Using CRN $mycrn<br>";
    $crn = $mycrn;
  }

  echo "Using Week $week<br>";

  $studarray = explode(";",$rawdata);
	?>
    <div id="wrapper">
    <div id="wrapper_content">
    
    <?php
    	
  echo "Please wait while processing ".count($studarray)." students...<br><br>";

  $i = 0;
  foreach ($studarray as $studid)
  {
   $qry = "INSERT INTO attendance (crn,week,studid,loginid) VALUES 
       (".$crn.",".$week.",upper('".$studid."'),$uid)";
   if (!$db->query($qry)) {
     echo "There was a problem with the following record:<br>";
     echo "(crn,week,studid,loginid) = ($crn,$week,$studid,$uid)<br>";
     echo "The error is: ".mysqli_error($db->link());
  } else {
    $i++;
  }
  }
  
   
   
?>
</div>
</div>

<?php
include 'footer.php';
?>

<script>
alert("Thank you, <?php echo $i;?> students successfully recorded");
//THIS IS DONE TO MAKE SURE THAT THIS PAGE IS NOT REFRESHED (WHICH WILL LEAD TO RE-INSERTION DATA)
window.location = "overview_attendance.php";
</script>

