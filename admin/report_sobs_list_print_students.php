<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SOB - Summary of students</title>

<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
<script src="js/jquery.js"></script>
<script src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>

<style>

body{
	font-family:Arial;
	font-size:12px;
	color:#525252;
}

h2{
	margin:0px;
	padding:0px;
}

h2 span{
	font-size:12px;
	font-weight:bold;
}

.print_view_wrapper{
	width:900px;
	margin:0px auto;
}

.content_table{
	background:#333;
}

.table_heading{
	background:#FFF;
	font-weight:bold;
}

.content_table tr{
	background:#FFF;
}

#graph_legend{
	height:30px;
	font-weight:bold;
}

#graph_legend ul{
	list-style: none outside none;
}

#graph_legend ul li{
float:right;
padding:5px 5px;
line-height:15px;
margin-right:10px;	
}


#graph_legend ul li a{
	text-decoration: none;
	color:#525252;
}

#graph_legend div{
float:left;

}

.legend_key{
width:15px;
height:15px;
margin-right:5px;	
}

.ideal{
background-color: #000000;	
}

.student_selected{
background-color: #008800;		
}

.other_students{
background-color: #EECA06;			
}

</style>

</head>

<body>

<div class="print_view_wrapper">
    <h2>SOB Report <span style="float:right;">Printed on : <?php echo date('l jS \of F Y h:i:s A');?></span></h2>
    
<br>
<br>

<?php
    if (isset($_GET['sob_id'])) {
      // We have provided a SOB ID.
      // FIXME: escape SQL injection
      $sob_id = $_GET['sob_id'];


      $qry = "SELECT * from sobs where sob_id = $sob_id";
      $sob_obj =  $db->query($qry);

      if ($sob_obj->num_rows == 0) {
	die("Error! Unknown SOB id!");
      }
      $sobs = $sob_obj->rows;
      $sob = $sobs[0];
      
      $sob_details = "<strong>SOB ID</strong> : ". $sob_id . 
	               "<br /> <strong>SOB</strong> : " . $sob['sob'] . "<br /><br /><br />";
      echo $sob_details;

      // These are the active students (status = 0) who have this SOB.
      $qry_with = "SELECT * FROM `students` WHERE student_id IN (SELECT student_id FROM sob_observations WHERE sob_id = $sob_id) AND student_status = 0";
      $qry_without = "SELECT * FROM `students` WHERE student_id NOT IN (SELECT student_id FROM sob_observations WHERE sob_id = $sob_id) AND student_status = 0";

      // We print first the student who have *not* been observed for this SOB.
      $students_obj = $db->query($qry_without);
      if ($students_obj->num_rows > 0) {
?>
<h2>List of students not yet observed</h2>
<br />
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25" align="left">No.</th>
          <th width="120" align="left">Student Number </th>
          <th align="left">Name </th>
          <th width="120" align="left">Email </th>
          <th width="120" align="left">Network ID</th>          
      </tr>
<?php
	$students = $students_obj->rows;
	$s=0;
	foreach($students as $student){	
	  $stud_id = $student['student_id'];
?>
	<tr>
	   <td align="left" valign="top"><?php $s++;echo $s;?></td>
	   <td align="left" valign="top"><?php echo $student['student_number'];?></Td>
                  <td valign="top" align="left"><?php echo $student['firstname'];?> <?php echo $student['lastname'];?></Td>
                  <td align="left" valign="top"><?php echo $student['email'];?></Td>
                  <td valign="top"><?php echo $student['network_name'];?></Td>
              </tr>
            <?php
          } // end foreach student
?>
	</table>
<?php
      } // end of if number of unobserved students > 0 for a SOB

      // We now get the students who have been observed
      $students_obj = $db->query($qry_with);
      if ($students_obj->num_rows > 0) {
?>
<br /><br />
      <h2>List of students already observed</h2>
      <br />
      <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25" align="left">No.</th>
          <th width="120" align="left">Student Number </th>
          <th align="left">Name </th>
          <th width="120" align="left">Email </th>
          <th width="120" align="left">Network ID</th>          
      </tr>
      <?php
	$students = $students_obj->rows;
	$s=0;
	foreach($students as $student){	
	  $stud_id = $student['student_id'];
?>
	<tr>
	   <td align="left" valign="top"><?php $s++;echo $s;?></td>
	   <td align="left" valign="top"><?php echo $student['student_number'];?></Td>
                  <td valign="top" align="left"><?php echo $student['firstname'];?> <?php echo $student['lastname'];?></Td>
                  <td align="left" valign="top"><?php echo $student['email'];?></Td>
                  <td valign="top"><?php echo $student['network_name'];?></Td>
              </tr>
            <?php
          } // end foreach student ?>
	</table>
<?php  } // end of if number of observed students > 0 for a SOB
   } // end of isset sob_id
?>
            <div class="clear"></div>
            <br />
</div>
</body>
</html>