<?php
session_start();
include 'login_checker.php';
include 'header.php';
include_once('db_class.php');


if(has_capabilities($uid, 'Manage Staffs')==false){
	header('Location:home.php');
	exit();
}


?>

<div id="wrapper">
    <div id="wrapper_content">
    	<h1 class="page_title">Manage Staff</h1>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
            	<td class="sub_headings" align="left">List of Staff</td>
                <td align="right"> <a class="small themebutton button" style="float:right;" onClick="add_new_staff()" href="javascript:;">Add Staff</a></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
        </table>
      


        <div id="page_contents">
			<?php include 'list_staffs.php';?>
      </div>
      
  </div>
</div>
<?php

include 'footer.php';
?>