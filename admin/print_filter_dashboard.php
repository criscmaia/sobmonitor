<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SOB - Staff</title>

<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.min.css" />
<script src="js/jquery.js"></script>
<script src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>

<style>

body{
	font-family:Arial;
	font-size:12px;
	color:#525252;
}

h2{
	margin:0px;
	padding:0px;
}

h2 span{
	font-size:12px;
	font-weight:bold;
}

.print_view_wrapper{
	width:900px;
	margin:0px auto;
}

.content_table{
	background:#333;
}

.table_heading{
	background:#FFF;
	font-weight:bold;
}

.content_table tr{
	background:#FFF;
}

#graph_legend{
	height:30px;
	font-weight:bold;
}

#graph_legend ul{
	list-style: none outside none;
}

#graph_legend ul li{
float:right;
padding:5px 5px;
line-height:15px;
margin-right:10px;	
}


#graph_legend ul li a{
	text-decoration: none;
	color:#525252;
}

#graph_legend div{
float:left;

}

.legend_key{
width:15px;
height:15px;
margin-right:5px;	
}

.ideal{
background-color: #000000;	
}

.student_selected{
background-color: #008800;		
}

.other_students{
background-color: #EECA06;			
}

</style>

<script>

$(document).ready(function() {
	show_all_student_record_compare();
});



function generate_graph(values, labels, colors, holder, max_y_value){
	$.jqplot.config.enablePlugins = true;
			var s1 = values;
			var ticks = labels;
			
			plot1 = $.jqplot(holder, [s1], {
				// Only animate if we're not using excanvas (not in IE 7 or IE 8)..
				animate: !$.jqplot.use_excanvas,
				seriesColors:colors,
				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					shadow : false,
					rendererOptions: {
					// Set varyBarColor to tru to use the custom colors on the bars.
					varyBarColor: true
				},
					pointLabels: { show: false }
				},
				axes: {
					xaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						ticks: ticks,
						label : 'Progress of all students Vs Progress expected by <?php echo date('d.m.Y');?>',
						labelOptions: {
							fontFamily: 'Arial',
							fontSize: '12px'
						  }
					},
					yaxis:{
						tickOptions:{
						formatString: "%d",
						fontSize: '12px'
						},
					   label:'Number of SOBs',
	   				   min:0,
  		            	max: max_y_value,
					   labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
					   labelOptions: {
							fontFamily: 'Arial',
							fontSize: '12px'
						  }
					}
				},
				highlighter: { show: true }
			});
			plot1.replot();
}

function show_all_student_record_compare(){
	var filters = '<?php echo $_SERVER["QUERY_STRING"];?>';
	var url = 'show_student_record_compare_dashboad.php?'+filters;
	$.getJSON(url, function(data){
		generate_graph(data.values, data.labels, data.colors, 'chart',data.max_y_value)
	});
}



</script>
</head>

<body>

<div class="print_view_wrapper">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td align="left">
    <h2>Progress Report <span style="float:right;">Printed on : <?php echo date('l jS \of F Y h:i:s A');?></span></h2>
    
    
    </td>
  </tr>
</table>

<br>
<br>
			
            

<?php
$stud_obj = $db->query("SELECT * FROM `students` WHERE `student_status` = 0 ");
$students = $stud_obj->rows;
		
		$flag = 0;
		
		if($_GET['levels']!=""){
			$levels = $_GET['levels'];
			$add_filter_levels = " AND level_id IN ($levels) ";
			$flag = 1;
			$levels_obj = $db->query("select * from `levels` where level_id IN ($levels)");
			$levels = $levels_obj->rows;
			$levels_txt = "";
			foreach($levels as $level){
				
				if($levels_txt=="")
					$levels_txt= $level['level'];
				else
					$levels_txt.=', '.$level['level'];
			}
			
			$filter_levels = $levels_txt;
		}
		else{
			$add_filter_topics = "";
			$filter_levels = 'All';
		}
		
		if($_GET['topics']!=""){
			$topics = $_GET['topics'];
			$add_filter_topics = " AND topic_id IN ($topics) ";
			$flag = 1;
			$topics_obj = $db->query("select * from `topics` where topic_id IN ($topics)");
			$topics = $topics_obj->rows;
			$topics_txt = "";
			foreach($topics as $topic){
				if($topics_txt=="")
					$topics_txt= $topic['topic'];
				else
					$topics_txt.=', '.$topic['topic'];
				
			}
			
			$filter_topics = $topics_txt;
		}
		else{
			$add_filter_topics = "";
			$filter_topics = 'All';
		}
		
		if($_GET['from_date']!=""){
			$from_date = date_mysql($_GET['from_date']);
			$add_filter_from =" AND expected_completion_date >= '$from_date' ";
			$flag = 1;
			
		}
		else{
			$add_filter_from = "";	
			
		}
		
		if($_GET['to_date']!=""){
			$to_date = date_mysql($_GET['to_date']);
			$add_filter_to =" AND expected_completion_date <= '$to_date' ";
			$flag = 1;
		}
		else{
			$add_filter_to = "";
		}
		
		if(isset($_GET['from_date']) && $_GET['from_date']!=""){
			$filter_date = 'Between '.$_GET['from_date'] . ' and '.$_GET['to_date'];
		}
		else{
			$filter_date =  'Any';
		}
		
		if(isset($_GET['observed_from']) && $_GET['observed_from']!=""){
			$filter_range = 'Between '.$_GET['observed_from'] . ' and '.$_GET['observed_to'];
		}
		else{
			$filter_range = 'Any';
		}
		
		$applied_filter = "<strong>Levels</strong> : $filter_levels | <strong>Topics</strong> : $filter_topics <br /><strong> Expected Completion Date</strong> : $filter_date | <strong>No of Observed SOBs</strong> : $filter_range";
?>
<b>Applied Filter :</b> <br />
	<?php echo $applied_filter;?>
    <br><br />

<h2>List of Students</h2>
<br />
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25" align="left">S.No</th>
          <th width="120" align="left">Student Number </th>
          <th align="left">Name </th>
          <th width="120" align="left">Email </th>
          <th width="120" align="left">Network ID</th>
          
          <th width="110" align="left">Student Status</th>
         <th width="100" align="left">Observed SOBs</th>
      </tr>

<?php
$s=0;
foreach($students as $student){
	
	$stud_id = $student['student_id'];
	$sql = " AND `sob_id` IN (SELECT sob_id FROM `sobs` WHERE 1 $add_filter_levels $add_filter_topics $add_filter_from $add_filter_to)";
		
		$filter_query = "SELECT COUNT(*) AS user_finished_total FROM `sob_observations` WHERE `student_id` = '$stud_id'";
		
		if($flag==1){
			$filter_query = $filter_query.$sql;
		}
	
	$user_finished_obj = $db->query($filter_query);
	$user_finished = $user_finished_obj->row;
	
	
	$user_finish = $user_finished['user_finished_total'];
	
	if(isset($_GET['observed_from']) && $_GET['observed_from']!=""){
		if($user_finish>=$_GET['observed_from'] && $user_finish<=$_GET['observed_to']){
			
			?>
             <tr>
                  <td align="left" valign="top"><?php $s++;echo $s;?></td>
                  <td align="left" valign="top"><?php echo $student['student_number'];?></Td>
                  <td valign="top" align="left"><?php echo $student['firstname'];?> <?php echo $student['lastname'];?></Td>
                  <td align="left" valign="top"><?php echo $student['email'];?></Td>
                  <td valign="top"><?php echo $student['network_name'];?></Td>
                  <td valign="top" id="student_status_<?php echo $student['student_id'];?>">
                  <?php 
                  if($student['student_status']=="1"){
                      echo 'Inactive';
                  }
                  else if($student['student_status']=="0"){
                      echo 'Active';
                  }
                  ?>
                  </Td>
                  <td><?php echo $user_finish;?></td>
              </tr>
            <?php
		}
	}
	else{
			?>
            <tr>
                  <td align="left" valign="top"><?php $s++;echo $s;?></td>
                  <td align="left" valign="top"><?php echo $student['student_number'];?></Td>
                  <td valign="top" align="left"><?php echo $student['firstname'];?> <?php echo $student['lastname'];?></Td>
                  <td align="left" valign="top"><?php echo $student['email'];?></Td>
                  <td valign="top"><?php echo $student['network_name'];?></Td>
                  <td valign="top" id="student_status_<?php echo $student['student_id'];?>">
                  <?php 
                  if($student['student_status']=="1"){
                      echo 'Inactive';
                  }
                  else if($student['student_status']=="0"){
                      echo 'Active';
                  }
                  ?>
                  </Td>
                  <td><?php echo $user_finish;?></td>
              </tr>
            <?php
	}
				
}

?>
</table>

<br />
<br />
<?php
			$legend_graph = '<div id="graph_legend">
                <ul>
					<li><div class="legend_key other_students"></div>All students</li>
					<li><div class="legend_key ideal"></div>Expected by '.date('d.m.Y').'</li>                
                </ul>
            </div>'; 
			?>
			<div class="canvas_holder">
                <div class="graph_display_details"><h2>Overall Progress</h2>
                    <?php echo $legend_graph;?>
                    <div class="clear"></div>
                </div>
                
                <div id="chart" style=" width:100%; height:450px;"></div>
            </div>
            <div class="clear"></div>
            <br />
</div>
</body>
</html>