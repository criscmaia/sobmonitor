function student_notes(student_no){
	var height = $(window).height();
	var url = "student_notes.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&student_no='+student_no);
}

function insert_student_note(){
	var flag = 0;
	if(document.getElementById('note_text').value==""){
		document.getElementById('note_text').style.borderColor="#FF0000";
		flag=1;
	}
	else{
		document.getElementById('note_text').style.borderColor="";
	}
	
	
	
	if(flag==0){
		var vals = $('#reply_form').serialize();
		$.post('insert_student_note.php', vals, function(response){
			$('#reply_table').append(response);
			document.getElementById('note_text').value=""
		});
	}
}

function add_student_tutor(){
	if(document.getElementById('student_tutor').value!=""){
		var sid = $('#student_no').val();
		var tid = $('#student_tutor').val();
		$.get('update_student_tutor.php', 'student_no='+sid+'&tutor_id='+tid, function(response){
			$('#tutor_'+sid).html(response);
			cover_close();
		});
	}
}

function student_report(student_no){
	var url = 'print_student_report.php?student_no='+student_no;
	window.open(url, "STUDENTNO", "menubar=1,resizable=1,scrollbars=1,width=1000,height=700,left=10,top=10");
}

function print_report_all(){
	var url = 'print_student_report_all.php';
	window.open(url, "STUDENTNO", "menubar=1,resizable=1,scrollbars=1,width=1000,height=700,left=10,top=10");
}



function add_new_student(){
	var height = $(window).height();
	var url = "add_new_student.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height);
}

function student_tutor(student_no){
	var height = $(window).height();
	var url = "student_tutor.php";
	grayOut(true,'grayOut_center_div',500);
	$('#grayOut_center_div').load(url,'height='+height+'&student_no='+student_no);
}

function change_setting(obj){
	if(obj.checked==true){
		var status = 1
	}
	else{
		var status = 0;
	}
	
	var name = obj.value;
	$('#hidden_div').load('change_setting.php?setting_name='+name+'&status='+status,function(){
		$.jGrowl("Updated authentication setting");
	});
}

function edit_student(student_id){
	var height = $(window).height();
	var url = "edit_student.php?student_id="+student_id;
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height);
}

function student_deactivate(student_id){
	var conf = confirm("Are you sure that you want to deactivate this student?");
	if(conf==true){
		$('#student_status_'+student_id).html('...').load('change_student_status.php?student_id='+student_id+'&status=1',function(){
			$('#student_row_'+student_id).attr('class','highlight_color')
		});
	}
}


function student_activate(student_id){
	var conf = confirm("Are you sure that you want to activate this student?");
	if(conf==true){
		$('#student_status_'+student_id).html('...').load('change_student_status.php?student_id='+student_id+'&status=0',function(){
			$('#student_row_'+student_id).removeAttr('class')
		});
	}
}


function add_student(){
	var flag=0;
	
	if(document.getElementById('network_name').value==""){
		document.getElementById('network_name').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('network_name').style.borderColor='';
	}
	
	if(document.getElementById('student_number').value==""){
		document.getElementById('student_number').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('student_number').style.borderColor='';
	}
	
	if(document.getElementById('firstname').value==""){
		document.getElementById('firstname').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('firstname').style.borderColor='';
	}
	
	if(document.getElementById('email').value==""){
		document.getElementById('email').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('email').style.borderColor='';
	}
	

	
	if(flag==0){
		document.sob_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}

function update_student(){
	var flag=0;
	
	if(document.getElementById('network_name').value==""){
		document.getElementById('network_name').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('network_name').style.borderColor='';
	}
	
	if(document.getElementById('student_number').value==""){
		document.getElementById('student_number').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('student_number').style.borderColor='';
	}
	
	if(document.getElementById('firstname').value==""){
		document.getElementById('firstname').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('firstname').style.borderColor='';
	}
	
	if(document.getElementById('email').value==""){
		document.getElementById('email').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('email').style.borderColor='';
	}
	

	
	if(flag==0){
		document.sob_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}
function after_student_submit(){
	$.jGrowl("Student added successfully");
	cover_close();
	$('#page_contents').html('Loading... Please wait...').load('list_students.php');
}


function after_student_update(){
	$.jGrowl("Student updated successfully");
	cover_close();
	$('#page_contents').html('Loading... Please wait...').load('list_students.php');
}
