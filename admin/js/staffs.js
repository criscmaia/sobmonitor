
function add_new_staff(){
	var height = $(window).height();
	var url = "add_new_staff.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height);
}


function add_staff(){
	var flag=0;
	
	if(document.getElementById('network_name').value==""){
		document.getElementById('network_name').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('network_name').style.borderColor='';
	}
	
	if(document.getElementById('firstname').value==""){
		document.getElementById('firstname').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('firstname').style.borderColor='';
	}
	
	if(document.getElementById('email').value==""){
		document.getElementById('email').style.borderColor='#FF0000';
		flag=1;
	}
	else{
		document.getElementById('email').style.borderColor='';
	}
	

	
	if(flag==0){
		document.sob_form.submit();
		//document.sob_form.reset();
	}
	else{
		$.jGrowl("Please check the highlighted fields");
	}
}

function after_staff_submit(){
	$.jGrowl("Staff added successfully");
	cover_close();
	$('#page_contents').html('Loading... Please wait...').load('list_staffs.php');
}


function edit_staff(staff_id){
	var url = 'staff_capabilities.php?staff_id='+staff_id;
	$.getJSON(url, function(data){
		var arr = data.info;
		$.each(arr, function(index, arr) {
			
			if(arr.access=="1")
			var checked = 'checked="checked"';
			else
			var checked = "";
			
			$('#capability_access_'+staff_id+'_'+arr.capability).html('<input type="checkbox" value="'+arr.capability+'" name="capability_'+staff_id+'" id="capability_'+staff_id+'_'+arr.capability+'" '+checked+'>');
		});
		$('#edit_button_'+staff_id).html('<a href="javascript:;" onclick="save_staff('+staff_id+')" title="Save Staff Capabilities"><img src="images/save.png" border="0" /></a>');
	});
}

function save_staff(staff_id){
	var arr = document.getElementsByName('capability_'+staff_id);
	
	var cap = "";
	var after = new Array();
	var boxs =  new Array();
	var len = arr.length;
	for(var i=0;i<arr.length;i++){
		
		var c = arr[i].value;
		var s = 0
		
		if(arr[i].checked==true){
			if(cap=="")
			cap=c;
			else
			cap+=","+c;
			
			s=1;
		}
		
		
		after.push(s);
			
		boxs.push(c);

		
	
	}
	$('#edit_button_'+staff_id).html('Saving...').load('save_staff_capabilities.php','staff_id='+staff_id+'&capabilites='+cap,function(){
		
		

		for(var i=0;i<len;i++){

			
			if(after[i]==1){
				var html = '<img src="images/tick.png" />';
			}
			else{
				var html = '<img src="images/nocapability.png" />';
			}
			
			$('#capability_access_'+staff_id+'_'+boxs[i]).html(html);
		}
		
	});
}