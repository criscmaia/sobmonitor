
function toggle_graph(divid,obj){
	
	if($(obj).html()=="+ Show"){
		$('#'+divid).slideDown();
		$(obj).html('- Hide');
	}
	else{
		$('#'+divid).slideUp();
		$(obj).html('+ Show');
	}
	
	
}

function cover_close() 
{  
	grayOut(false,'grayOut_center_div');
}

function full_close() 
{  
	full_grayout(false);
}

function sidebar_close() 
{  
	grayOut_sidebar(false,'grayOut_sidebar');
	document.getElementById('close_div_sidebar').style.display = 'none';
	
}

//grayOut
function grayOut(vis, divname,div_width) 
{
	if(div_width==null)
	{
		div_width = 600;
	}
	document.getElementById(divname).style.width=div_width+"px";
	
	
	var w = parseFloat(document.body.scrollWidth);
	var h = parseFloat(document.body.clientHeight);
	
	 var viewportwidth;
	 var viewportheight;
	
	if (typeof window.innerWidth != 'undefined')
	{
	  viewportwidth = window.innerWidth,
	  viewportheight = window.innerHeight
	}
	else if(typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)
	{
		// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
	   viewportwidth = document.documentElement.clientWidth,
	   viewportheight = document.documentElement.clientHeight
	} 
	else
	{
		// older versions of IE
		viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
		viewportheight = document.getElementsByTagName('body')[0].clientHeight
	}
	
	var top= 80;
	var half_div_width = parseInt(div_width)/2;
	var le  = (parseInt(viewportwidth)/2) - half_div_width;
	document.getElementById(divname).style.left = le+'px';
	document.getElementById(divname).style.top = top+'px';
	
	var left = le+(parseInt(div_width)-12)
	document.getElementById('grayout_close').style.left = left+'px';
	document.getElementById('grayout_close').style.top = (top-25)+'px';
	
	if(vis == true)
	{
		document.getElementById(divname).style.display = 'block';
		document.getElementById('grayout_close').style.display = 'block';
	}
	else
	{
		document.getElementById(divname).style.display = 'none';
		document.getElementById('grayout_close').style.display = 'none';
		document.getElementById(divname).innerHTML = '';
	}
  var options = options || {}; 
  var zindex = options.zindex || 1002;
  var opacity = options.opacity || 70;
  var opaque = (opacity / 100);
  var bgcolor = options.bgcolor || '#000000';
  var dark=document.getElementById('darkenScreenObject');
  if (!dark) {
    // The dark layer doesn't exist, it's never been created.  So we'll
    // create it here and apply some basic styles.
    // If you are getting errors in IE see: http://support.microsoft.com/default.aspx/kb/927917
    var tbody = document.getElementsByTagName("body")[0];
    var tnode = document.createElement('div');           // Create the layer.
        tnode.style.position='fixed';                 // Position absolutely
        tnode.style.top='0px';                           // In the top
        tnode.style.left='0px';                          // Left corner of the page
        tnode.style.overflow='hidden';                   // Try to avoid making scroll bars            
        tnode.style.display='none';                      // Start out Hidden
        tnode.id='darkenScreenObject';                   // Name it so we can find it later
    tbody.appendChild(tnode);                            // Add it to the web page
    dark=document.getElementById('darkenScreenObject');  // Get the object.
  }
  if (vis) {
    // Calculate the page width and height 
    if( document.body && ( document.body.scrollWidth || document.body.scrollHeight ) ) {
        var pageWidth = document.body.scrollWidth+'px';
        var pageHeight = document.body.scrollHeight+'px';
    } else if( document.body.offsetWidth ) {
      var pageWidth = document.body.offsetWidth+'px';
      var pageHeight = document.body.offsetHeight+'px';
    } else {
       var pageWidth='100%';
       var pageHeight='100%';
    }   
    //set the shader to cover the entire page and make it visible.
	pageHeight = $(document).height();
	var viewH = $(window).height();
	if(parseFloat(pageHeight)<parseFloat(viewH))
	{
		var fixH = viewH+'px';
	}
	else
	{
		var fixH = pageHeight+'px';
	}
	
    dark.style.opacity=opaque;                      
    dark.style.MozOpacity=opaque;                   
    dark.style.filter='alpha(opacity='+opacity+')'; 
    dark.style.zIndex=zindex;        
    dark.style.backgroundColor=bgcolor;  
    dark.style.width= '100%';
    dark.style.height=  pageHeight+'px';
    dark.style.display='block';                          
  } else {
     dark.style.display='none';
  }
 
}

//grayOut_sidebar
function grayOut_sidebar(vis, divname) 
{
	
		var div_width = 300;
		var div_height = $(window).height();
	
	document.getElementById(divname).style.width=div_width+"px";
	document.getElementById(divname).style.height=div_height+"px";
	
	var w = parseFloat(document.body.scrollWidth);
	var h = parseFloat(document.body.clientHeight);
	
	 var viewportwidth;
	 var viewportheight;
	
	if (typeof window.innerWidth != 'undefined')
	{
	  viewportwidth = window.innerWidth,
	  viewportheight = window.innerHeight
	}
	else if(typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)
	{
		// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
	   viewportwidth = document.documentElement.clientWidth,
	   viewportheight = document.documentElement.clientHeight
	} 
	else
	{
		// older versions of IE
		viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
		viewportheight = document.getElementsByTagName('body')[0].clientHeight
	}
	
	var top = 0;
	var right = 0;
	document.getElementById(divname).style.right = right+'px';
	document.getElementById(divname).style.top = top+'px';
	
	if(vis == true)
	{
		document.getElementById(divname).style.display = '';
		document.getElementById('close_div_sidebar').style.display = 'block';
	}
	else
	{
		document.getElementById(divname).style.display = 'none';
		document.getElementById('close_div_sidebar').style.display = 'none';
		document.getElementById(divname).innerHTML = '';
	}
  var options = options || {}; 
  var zindex = options.zindex || 1002;
  var opacity = options.opacity || 70;
  var opaque = (opacity / 100);
  var bgcolor = options.bgcolor || '#000000';
  var dark=document.getElementById('darkenScreenObject');
  if (!dark) {
    // The dark layer doesn't exist, it's never been created.  So we'll
    // create it here and apply some basic styles.
    // If you are getting errors in IE see: http://support.microsoft.com/default.aspx/kb/927917
    var tbody = document.getElementsByTagName("body")[0];
    var tnode = document.createElement('div');           // Create the layer.
        tnode.style.position='fixed';                 // Position absolutely
        tnode.style.top='0px';                           // In the top
        tnode.style.left='0px';                          // Left corner of the page
        tnode.style.overflow='hidden';                   // Try to avoid making scroll bars            
        tnode.style.display='none';                      // Start out Hidden
        tnode.id='darkenScreenObject';                   // Name it so we can find it later
    tbody.appendChild(tnode);                            // Add it to the web page
    dark=document.getElementById('darkenScreenObject');  // Get the object.
  }
  if (vis) {
    // Calculate the page width and height 
    if( document.body && ( document.body.scrollWidth || document.body.scrollHeight ) ) {
        var pageWidth = document.body.scrollWidth+'px';
        var pageHeight = document.body.scrollHeight+'px';
    } else if( document.body.offsetWidth ) {
      var pageWidth = document.body.offsetWidth+'px';
      var pageHeight = document.body.offsetHeight+'px';
    } else {
       var pageWidth='100%';
       var pageHeight='100%';
    }   
    //set the shader to cover the entire page and make it visible.
	pageHeight = $(document).height();
	var viewH = $(window).height();
	if(parseFloat(pageHeight)<parseFloat(viewH))
	{
		var fixH = viewH+'px';
	}
	else
	{
		var fixH = pageHeight+'px';
	}
	
    dark.style.opacity=opaque;                      
    dark.style.MozOpacity=opaque;                   
    dark.style.filter='alpha(opacity='+opacity+')'; 
    dark.style.zIndex=zindex;        
    dark.style.backgroundColor=bgcolor;  
    dark.style.width= '100%';
    dark.style.height= '100%';
    dark.style.display='block';                          
  } else {
     dark.style.display='none';
  }
 
}

//full_grayout
function full_grayout(vis) 
{
	var divname ='full_grayout';
	
	var width = $(window).width();
	var div_width = parseFloat(width) - 100;
	
	var height = $(window).height();
	var div_height = parseFloat(height) - 100;
	
	document.getElementById(divname).style.width=div_width+"px";
	document.getElementById(divname).style.height=div_height+"px";
	
	var w = parseFloat(document.body.scrollWidth);
	var h = parseFloat(document.body.clientHeight);
	
	 var viewportwidth;
	 var viewportheight;
	
	if (typeof window.innerWidth != 'undefined')
	{
	  viewportwidth = window.innerWidth,
	  viewportheight = window.innerHeight
	}
	else if(typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)
	{
		// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
	   viewportwidth = document.documentElement.clientWidth,
	   viewportheight = document.documentElement.clientHeight
	} 
	else
	{
		// older versions of IE
		viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
		viewportheight = document.getElementsByTagName('body')[0].clientHeight
	}
	

	
	document.getElementById(divname).style.left = '50px';
	document.getElementById(divname).style.top = '50px';
	
	if(vis == true)
	{
		document.getElementById(divname).style.display = 'block';
		document.getElementById('close_div').style.display = 'block';
	}
	else
	{
		document.getElementById(divname).style.display = 'none';
		document.getElementById('close_div').style.display = 'none';
		document.getElementById(divname).innerHTML = '';
	}
  var options = options || {}; 
  var zindex = options.zindex || 1002;
  var opacity = options.opacity || 70;
  var opaque = (opacity / 100);
  var bgcolor = options.bgcolor || '#000000';
  var dark=document.getElementById('darkenScreenObject');
  if (!dark) {
    // The dark layer doesn't exist, it's never been created.  So we'll
    // create it here and apply some basic styles.
    // If you are getting errors in IE see: http://support.microsoft.com/default.aspx/kb/927917
    var tbody = document.getElementsByTagName("body")[0];
    var tnode = document.createElement('div');           // Create the layer.
        tnode.style.position='fixed';                 // Position absolutely
        tnode.style.top='0px';                           // In the top
        tnode.style.left='0px';                          // Left corner of the page
        tnode.style.overflow='hidden';                   // Try to avoid making scroll bars            
        tnode.style.display='none';                      // Start out Hidden
        tnode.id='darkenScreenObject';                   // Name it so we can find it later
    tbody.appendChild(tnode);                            // Add it to the web page
    dark=document.getElementById('darkenScreenObject');  // Get the object.
  }
  if (vis) {
    // Calculate the page width and height 
    if( document.body && ( document.body.scrollWidth || document.body.scrollHeight ) ) {
        var pageWidth = document.body.scrollWidth+'px';
        var pageHeight = document.body.scrollHeight+'px';
    } else if( document.body.offsetWidth ) {
      var pageWidth = document.body.offsetWidth+'px';
      var pageHeight = document.body.offsetHeight+'px';
    } else {
       var pageWidth='100%';
       var pageHeight='100%';
    }   
    //set the shader to cover the entire page and make it visible.
	pageHeight = $(document).height();
	var viewH = $(window).height();
	if(parseFloat(pageHeight)<parseFloat(viewH))
	{
		var fixH = viewH+'px';
	}
	else
	{
		var fixH = pageHeight+'px';
	}
	
    dark.style.opacity=opaque;                      
    dark.style.MozOpacity=opaque;                   
    dark.style.filter='alpha(opacity='+opacity+')'; 
    dark.style.zIndex=zindex;        
    dark.style.backgroundColor=bgcolor;  
    dark.style.width= '100%';
    dark.style.height= pageHeight+'px';
    dark.style.display='block';                          
  } else {
     dark.style.display='none';
  }
 
}

function print_disengagement_report(){
	var url = 'print_disengagement_report.php';
	window.open(url, "Disengagement report", "menubar=1,resizable=1,scrollbars=1,width=1000,height=700,left=10,top=10");
}
