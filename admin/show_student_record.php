<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('login_checker.php');

$color_array = array("","#FF0000", "#f47901", "#108b03","#6e5134");

$stud_id = $_GET['student_id'];
$stud_obj = $db->query("SELECT * FROM `students` WHERE `student_number` = '$stud_id'");
$stud_no = $stud_obj->num_rows;
extract($_GET);
if($stud_no==0){
	
	?>
		<table width="600" class="content_table" border="0" cellpadding="10" cellspacing="1">
		 <tr>
			<Td align="center"><b style="color:#F00">Invalid Student ID</b></Td>
		  </tr>
		</table>
    <?php
	
}
else{
	$stud_details = $stud_obj->row;
	$student_id = $stud_details['student_id'];
	
	?>
			<table width="100%" cellpadding="0" cellspacing="0">
	
				<tr>
					<td class="sub_headings" align="left"><span><?php echo $stud_details['firstname'];?> <?php echo $stud_details['lastname'];?> (<?php echo $stud_details['student_number'];?>)</span> </td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				</tr>
	
			</table>

		<?php
		$today = date('Y-m-d');
		$level_obj = $db->query("select DISTINCT l.level_id, l.level from `sobs` s, `levels` l where s.level_id = l.level_id ORDER BY l.level_id");
		$levels = $level_obj->rows;
		
		foreach($levels as $level){
			$level_id = $level['level_id'];
			$total_obj = $db->query("SELECT COUNT(*) AS total FROM `sobs` s, `levels` l WHERE s.level_id = l.level_id AND l.level_id = '$level_id'");
			$tot = $total_obj->row;
			
			$user_tot_obj = $db->query("SELECT COUNT(*) AS user_total FROM `sobs` s, `levels` l WHERE s.level_id = l.level_id AND l.level_id = '$level_id' AND s.expected_completion_date <= '$today'");
			$user_tot = $user_tot_obj->row;
			
			$user_finished_obj = $db->query("SELECT COUNT(*) AS user_finished_total FROM `sob_observations` WHERE `student_id` = '$student_id' AND `sob_id` IN (SELECT sob_id FROM `sobs` WHERE `level_id` = '$level_id') AND (observed_on <= '$today' AND observed_on != '0000-00-00')");
			$user_finished = $user_finished_obj->row;
			
			$total = $tot['total']; 
			$user_total = $user_tot['user_total']; 
			$user_finish = $user_finished['user_finished_total'];
			
			$achieved = ($user_finish/$total)*100;
			$achievement = ($user_total/$total)*100;
			?>
            <b class="level_display"><?php echo $level['level'];?></b>
            <div class="process_bar">
            	<div class="achieved_bar" style="width:<?php echo $achieved;?>%;"></div>
                <div class="expected_achievement" style="width:<?php echo $achievement;?>%;"></div>
                <div class="level_legend">
                	<table width="330" border="0" cellspacing="0" cellpadding="0" align="right">
                      <tr>
                        <td bgcolor="#C8C7C7" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Total : ' . $total;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#008800" width="10">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Observed : ' . $user_finish;?>&nbsp;&nbsp;</td>
                        <td bgcolor="#000000" width="1">&nbsp;</td>
                        <td>&nbsp;<?php echo 'Expected : ' . $user_total;?>&nbsp;&nbsp;</td>
                      </tr>
                    </table>

                </div>
            </div>
            <?php
		}
}
?>