<?php
session_start();
include_once('login_checker.php');

if(has_capabilities($uid, 'Attendance')==false){
	header('Location:home.php');
	exit();
}

extract($_GET);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SOB - Staff</title>
<script src="js/jquery.js"></script>
<style>

body{
	font-family:Arial;
	font-size:12px;
	color:#525252;
}

h2{
	margin:0px;
	padding:0px;
}

h2 span{
	font-size:12px;
	font-weight:bold;
}

.print_view_wrapper{
	width:900px;
	margin:0px auto;
}

.content_table{
	background:#333;
}

.table_heading{
	background:#FFF;
	font-weight:bold;
}

.content_table tr{
	background:#FFF;
}

.heading{
	font-size:14px;
	font-weight:bold;
}
#graph_legend{
	height:30px;
	font-weight:bold;
}

#graph_legend ul{
	list-style: none outside none;
}

#graph_legend ul li{
float:right;
padding:5px 5px;
line-height:15px;
margin-right:10px;	
}


#graph_legend ul li a{
	text-decoration: none;
	color:#525252;
}

#graph_legend div{
float:left;

}

.legend_key{
width:15px;
height:15px;
margin-right:5px;	
}


.other_students{
background-color: #EAEAEA;			
}


.highlight{
background-color: #EAEAEA !important;			
}
</style>

</head>

<body>

<div class="print_view_wrapper">
<table width="100%" border="0" cellspacing="0" cellpadding="6">
  <tr>
    <td align="left">
    <h2>Attendance : CRN - <?php echo $crn;?> | Week - <?php echo $week;?><span style="float:right;">Printed on : <?php echo date('l jS \of F Y h:i:s A');?></span></h2>
    <?php
	 $print_crn = $db->query("SELECT * FROM `CRNlist` WHERE crn = '$crn'");
 	 $crn_detail = $print_crn->row;
	?>
    <p class="heading"><?php echo $crn_detail['codetype'];?> - <?php echo $crn_detail['room'];?> - <?php echo $crn_detail['day'];?> (<?php echo $crn_detail['starttime'];?> - <?php echo $crn_detail['endtime'];?>)</p>
    </td>
  </tr>
</table>
<br>
<div id="graph_legend" style="float:right;">
                <ul>
					<li><div class="legend_key other_students"></div>Students not registered for this CRN OR Student Info missing</li>              
                </ul>
            </div>
<p class="heading">Present</p>
			
<table width="100%" border="0" cellspacing="1" cellpadding="10"  class="content_table">
<tr class="table_heading">
	<th width="50">S.No</th>
    <th width="130">Student Number</th>
    <th width="300">First Name</th>
    <th>Last Name</th>
</tr>
<?php
$prese=0;
 $query = $db->query("SELECT * FROM `attendance` as a, `students` as s WHERE a.crn = '$crn' AND a.studid = s.student_number AND a.week = '$week' ORDER BY s.firstname");
 $list_students = $query->rows;
 foreach($list_students as $list_student){
	 $s++;
	 
	
	 ?>
     <tr id="<?php echo $list_student['student_number'];?>" class="highlight">
     	<td><?php echo $s;?></td>
        <td><?php echo $list_student['student_number'];?></td>
        <td><?php echo $list_student['firstname'];?></td>
		<td><?php echo $list_student['lastname'];?></td>
     </tr>
     <?php
 }
 
 
 $query = $db->query("SELECT *  FROM `attendance` WHERE `crn` = '$crn' AND week = '$week' AND `studid` NOT IN (select DISTINCT student_number from students)");
 $list_students = $query->rows;
 foreach($list_students as $list_student){
	 $s++;
	 
	
	 ?>
     <tr id="<?php echo $list_student['studid'];?>" class="highlight">
     	<td><?php echo $s;?></td>
        <td><?php echo $list_student['studid'];?></td>
        <td >-</td>
		<td >-</td>
     </tr>
     <?php
 }
 ?>
 
 
 </table>
 
 
 
 
 <br />
<br />

<div id="missing_student_number"></div>
<script>
var sno=0;
var flag = 0;
var html = '<table width="100%" border="0" cellspacing="1" cellpadding="10"  class="content_table"><tr class="table_heading"><th width="50">S.No</th>	 <th width="130">Student Number</th>   <th width="300">First Name</th>    <th>Last Name</th></tr>';
<?php
$chk = $db->query("SELECT DISTINCT s.student_number, s.firstname, s.lastname FROM `student_timetable` as t, `students` as s WHERE t.crn ='$crn' AND t.student_number = s.student_number");
$student_numbers = $chk->rows;
foreach($student_numbers as $student_number){
	?>
	
	if(document.getElementById('<?php echo $student_number['student_number'];?>')){
		$('#<?php echo $student_number['student_number'];?>').removeClass('highlight');
	}
	else{
		sno++;
		html+='<tr><td>'+sno+'</td><td><?php echo $student_number['student_number'];?></td><td><?php echo str_replace("'","&apos;",$student_number['firstname']);?></td><td><?php echo $student_number['student_number'];?></td><td><?php echo str_replace("'","&apos;",$student_number['lastname']);?></td></tr>';
		
	}
	<?php
}
?>
html+='</table>';
var highlights = $('.highlight');

if(highlights.length==0){
	$('#graph_legend').hide()
}
$('#missing_student_number').html('<p class="heading">Absent</p>'+html);

</script>

<?php
//MISSING STUDENTS 

?>



</div>



</body>
</html>