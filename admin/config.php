<?php
// db properties
if($_SERVER['SERVER_NAME'] == 'localhost' ){
	// This is used for local testing
date_default_timezone_set('Europe/London');
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'CHANGEME');
define('DB_PASSWORD', 'CHANGEME');
define('DB_DATABASE', 'CHANGEME');
define('DB_PREFIX', '');
}
else{
// This is used when running on a server (not localhost)
date_default_timezone_set('Europe/London');
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'CHANGEME');
define('DB_PASSWORD', 'CHANGEME');
define('DB_DATABASE', 'CHANGEME');
define('DB_PREFIX', '');
}


$timeout_duration = 7200;

if(!function_exists(date_ft))
{
	function date_ft($mysql_date)
	{
		$indian_date = date('d.m.Y',strtotime("$mysql_date"));
		return $indian_date;
	}
}

if(!function_exists(num_format))
{
	function num_format($amount)
	{
		return number_format($amount,2);
	}	
}

if(!function_exists(date_mysql))
{
	function date_mysql($indian_date)
	{
		$mysql_dates = date('Y-m-d',strtotime("$indian_date"));
		return $mysql_dates;
	}
}

if(!function_exists(mysql_to_indian_date))
{
	function mysql_to_indian_date($mysql_date)
	{
		if($mysql_date=="0000-00-00")
		{
			return "";
		}
		else
		{
			$indian_date = date('d.m.Y',strtotime("$mysql_date"));
			return $indian_date;
		}
	}
}

if(!function_exists(DayDifference))
{
	function DayDifference($start, $end) {
	  $start = strtotime($start);
	  $end = strtotime($end);
	  $difference = $end - $start;
	  return round($difference / 86400);
	}
}



if(!function_exists(time_between))
{
	function time_between($from,$to,$current)
	{	
		$time = $current;
		$date = date('Y-m-d') . ' ' . $time;
		
		if(date($from)>date($to))
		{
			if(date($time)<date($to))
			{
				$from = date('Y-m-d',strtotime("-1 day")) . ' ' . $from;
				$to = date('Y-m-d') . ' ' . $to;
			}
			else
			{
				$to = date('Y-m-d',strtotime("+1 day")) . ' ' . $to;
				$from = date('Y-m-d') . ' ' . $from;
			}
			
		}
		else
		{
			$from = date('Y-m-d') . ' ' . $from;
			$to = date('Y-m-d') . ' ' . $to;
		}
		
		if($date>$from && $date<$to) {
			return "opened";
		} else {
			return 'closed';
		}	
	}
}


if(!function_exists(has_capabilities))
{
	function has_capabilities($logged_in_id, $capability){
		
		if(!isset($GLOBALS['db']))
		$db = new MySQL_Class(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		else
		$db = $GLOBALS['db'];
		
		$chk_cap = $db->query("SELECT * FROM `capabilities` as c, `capability_mapping` as m WHERE c.capability_id = m.capability_id AND m.staff_id = '$logged_in_id' AND c.capability_description = '$capability'");
		$chk = $chk_cap->num_rows;
	
		if($chk==1){
			return true;
		}
		else if($chk==0){
			return false;
		}
		else{
			return false;
		}
	
	}
}

?>