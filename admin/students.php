<?php
session_start();
include 'login_checker.php';
include 'header.php';
?>
<style>
.actions_list {
    -moz-font-feature-settings: inherit;
    -moz-font-language-override: inherit;
    background: none repeat scroll 0 0 #eaeaea;
    border: 1px solid #dcdcdc;
    border-radius: 5px;
    color: #525252;
    cursor: pointer;
    display: inline-block;
    font-family: inherit;
    font-size: inherit;
    font-size-adjust: inherit;
    font-stretch: inherit;
    font-style: inherit;
    font-variant: inherit;
    font-weight: inherit;
    height: 15px;
    line-height: 13px;
    margin-bottom: 4px;
    padding: 1px 1px 1px 10px;
    width: 80px;
}
.actions_dropdown {
    height: 16px;
    width: 16px;
}

.actions_list_div {
    background: url("images/down_arrow_blue.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
    display: none;
    float: right;
    height: 16px;
    margin-top: 5px;
    width: 16px;
}


.actions_process_div {
    bottom: 0;
    display: none;
    position: relative;
    right: 0;
	top:10px;
	
}


.actions_process_div div {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: none repeat scroll 0 0 #FFFF99;
   
    border:1px solid #EE3224;
	border-top:0px;
    height: auto;
    position: absolute;
    right: -11px;
    top: 0;
    width: 140px;
    z-index: 1;
}

.actions_process_div div a{
	display:block;
	padding:10px;
	text-decoration:none;
	color:#333;
}

.actions_process_div div a:hover{
	background:#FFF;
	
	
}

.actions_list_div.wait {
    display: block;
}

.process_td:hover .actions_process_div
{
	display:block;
}


</style>
<script>
function sorting_field_name(field)
{
	var curr_sort = $('#sorting_field_name').val();
	var sort_by = $('#sorting_by').val();

	if(curr_sort == field){
		if(sort_by=='ASC')
	$('#sorting_by').val('DESC');
	else
	$('#sorting_by').val('ASC');	
	}
	
	else{
	$('#sorting_field_name').val(field);
	$('#sorting_by').val('ASC');
	}
	sort_student_list();
	
	
}

function sort_student_list(){
$('#page_contents').load('list_students.php',$('#student_list_form').serialize(),function(response){
																						
	});	
}


</script>
<form id="student_list_form" name="student_list_form" >
<input type="hidden" name="sorting_field_name" id="sorting_field_name" value="s.level_id, s.topic_id" />
<input type="hidden" name="sorting_by" id="sorting_by" value="ASC" />
</form>
<div id="wrapper">
    <div id="wrapper_content">
    <h1 class="page_title">Manage Students</h1>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
            	<td class="sub_headings" align="left">List of Students</td>
                <td align="right"> 
                
                <?php
				if(has_capabilities($uid,'Manage Students')==true){
					?>
                	<a class="small themebutton button" style="float:right; margin-left:15px;" onClick="add_new_student()" href="javascript:;">Add Student</a>
					<?php
				}
				?>
                <a class="small themebutton button" style="float:right;" onClick="print_report_all()" href="javascript:;">Print report for all tutees</a>
				</td>
            </tr>
            <?php
			$login_query = $db->query("SELECT * FROM settings WHERE `setting_name` = 'LDAP_login'");
			$login_row = $login_query->row;
			$login_method = $login_row['setting_value'];
			?>
            <tr style="display:none;">
              <td colspan="2" align="right">
              <input type="checkbox" id="setting_name" name="setting_name" value="LDAP_login" onchange="change_setting(this)" <?php echo ($login_method=="1")? 'checked="checked"':'';?> /> Authenticate through LDAP
              </td>
            </tr>
        </table>
      
<br />


        <div id="page_contents">
            <?php
			$sorting_field_name ='lname';
			$sorting_by = 'ASC';
            include 'list_students.php';
            ?>
      </div>
      
  </div>
</div>
<?php
include 'footer.php';
?>