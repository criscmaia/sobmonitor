<?php
session_start();
include_once 'login_checker.php';
include 'header.php';

if(has_capabilities($uid, 'Attendance')==false){
	header('Location:home.php');
	exit();
}

include('weeks.php');

//curweek()
extract($_GET);

if(isset($fromweek) && $fromweek!=""){
	$from = $fromweek;
}
else{
	$from = 1;
}

if(isset($toweek) && $toweek!=""){
	$to = $toweek;
}
else{
	$to = 24;
}
?>
<script>
function recalculate(){
	var from = document.getElementById('week_from').value;
	var to = document.getElementById('week_to').value;
	if(parseInt(from)<=parseInt(to)){
		var url = "overview_attendance.php?fromweek="+from+"&toweek="+to;
		window.location=url;
	}
}

function print_attendance(crn,week){
	var url = 'print_attendance.php?crn='+crn+'&week='+week;
	window.open(url, "PRINT_ATTENDANCE", "menubar=1,resizable=1,scrollbars=1,width=1000,height=700,left=10,top=10");
}

</script>
<div id="wrapper">
    <div id="wrapper_content">
        <h1 class="page_title">Attendance Overview</h1>
<br />
<table width="310" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Week From</td>
    <td>
    <select id="week_from" name="week_from">
     <?php
		for($i=1;$i<=24;$i++){
			?>
            <option value="<?php echo $i;?>" <?php if($from==$i) echo 'selected="selected"';?>><?php echo $i;?></option>
            <?php
		}
		?>
    </select>
    </td>
    <td>To</td>
    <td><select id="week_to" name="week_to">
     <?php
		for($i=1;$i<=24;$i++){
			?>
            <option value="<?php echo $i;?>" <?php if($to==$i) echo 'selected="selected"';?>><?php echo $i;?></option>
            <?php
		}
		?>
    </select></td>
    <td><a href="javascript:;" class="small themebutton button" onClick="recalculate()"><span>Recalulate</span></a></td>
  </tr>
</table>

<br />
      <table width="100%" border="0" cellspacing="1" cellpadding="10" class="content_table">
      <tr class="table_heading">
        <th width="60">CRN</th>
        <th width="230">Details</th>
        <th width="50">Expected</th>
        <?php
		for($i=$from;$i<=$to;$i++){
			?>
            <th>W<?php echo $i;?></th>
            <?php
		}
		?>
      </tr>
      <?php
	  $query = $db->query("SELECT c.crn, c.codetype, c.day, c.room, c.starttime, c.endtime, count(s.crn) as expected FROM `CRNlist` as c, `student_timetable` as s WHERE c.crn = s.crn group by c.crn");
	  
	  $crn_list = $query->rows;
		  
	  foreach($crn_list as $crn){
		  $crn_no = $crn['crn'];
		  ?>
      <tr>
        <td><?php echo $crn['crn'];?></td>
        <td><?php echo $crn['codetype'];?> - <?php echo $crn['room'];?> - <?php echo $crn['day'];?> (<?php echo $crn['starttime'];?> - <?php echo $crn['endtime'];?>)</td>
        <td><?php echo $crn['expected'];?></td>
        <?php
		$attendees_obj = $db->query("SELECT w.week_number, count(a.week) as attendees FROM `CRNlist` as c, `attendance` as a, `week` as w WHERE  c.crn = a.crn and c.crn = '$crn_no' and w.week_number = a.week and w.week_number between $from and $to group by w.week_number ORDER BY w.week_number ASC");
		$attendees_arr = $attendees_obj->rows;
		$attendee = array();
		reset($attendee);
		
		foreach($attendees_arr as $attendees ){
			$attendee[$attendees['week_number']] = $attendees['attendees'];
	
		}

		
		for($i=$from;$i<=$to;$i++){
			if($attendee[$i]!=""){
				$no_of_students_attended = '<a href="javascript:;" onclick="print_attendance(\''.$crn_no.'\',\''.$i.'\')">'.$attendee[$i].'</a>';
			}
			else{
				$no_of_students_attended = 0;
			}
			?>
            <td><?php echo $no_of_students_attended;?></td>
            <?php
		}
		?>
      </tr>
      	<?php
	  }
	  ?>
    </table>

 
        </div>
    </div>

</div>
<?php
include 'footer.php';
?>
