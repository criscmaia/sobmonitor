<?php
session_start();
include_once('login_checker.php');




function time_elapsed_string($ptime)
{
    $etime = time() - $ptime;

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SOB - Student Report</title>
<link href="css/reports.css" rel="stylesheet" type="text/css" />
</head>

<body>

<?php
//moodle_imported_students :  student_id 	student_name 	student_number 	student_email 	last_login 	import_instance_id 
// moodle_import : import_instance_id 	date_time 	imported_by
//attendance: attendance_id 	crn 	week 	studid 	loginid 	record_timestamp 

/*REPORTS :
A report with students in Moodle and not in sobmonitor
followed by students who have not attended and not logged into moodle
followed by students who have not attended but logged into moodle (sort reverse on Moodle access)
followed by students who have not logged into moodle but have attended (I don't know how to sort them)...

I'd like a fourth table of people who are at "Disengagement risk".
Essentially, imagine you sort the students by reverse order of moodle log-in
remove the students who are in the first three table
present the top 15 students
And then a final table: students who have not attended and have not logged in during the previous week
*/



$student_query = $db->query("SELECT student_id, students.network_name as network_name, students.firstname as firstname, students.lastname as lastname, students.email as email, student_number, last_login, last_activity, student_status, staffs.firstname as staff_firstname, staffs.lastname as staff_lastname FROM `students`,staffs WHERE students.staff_id=staffs.staff_id");
$students_in_sob = $student_query->rows;

foreach($students_in_sob as $each_student_in_sob){

if($each_student_in_sob['student_status'] == 0){	
  $sob_array[$each_student_in_sob['student_number']] = array ( 'student_firstname' => $each_student_in_sob['firstname'],'student_lastname' => $each_student_in_sob['lastname'], 'student_email' => $each_student_in_sob['email'], 'student_status' => $each_student_in_sob['student_status'], 'student_tutor_name' => $each_student_in_sob['staff_firstname'], 'student_tutor_lastname' => $each_student_in_sob['staff_lastname']); 
}


$sob_all_students_array[$each_student_in_sob['student_number']] = array ( 'student_firstname' => $each_student_in_sob['firstname'],'student_lastname' => $each_student_in_sob['lastname'], 'student_email' => $each_student_in_sob['email'], 'student_status' => $each_student_in_sob['student_status']); 






}



$student_query = $db->query("SELECT * FROM `moodle_imported_students`");
$students_in_moodle = $student_query->rows;

foreach($students_in_moodle as $each_student_in_moodle){
	
	
if($each_student_in_moodle['last_login'] != '0000-00-00 00:00:00'){
$loggedin_unix = strtotime($each_student_in_moodle['last_login']);
$loggedin_time = date("d.m.Y", strtotime($each_student_in_moodle['last_login'])) .' at '.date("H:i", strtotime($each_student_in_moodle['last_login']));
}

else{
$loggedin_unix = '0';
$loggedin_time = 'Never';	
}

$moodle_array[$each_student_in_moodle['student_number']] = array ( 'student_name' => $each_student_in_moodle['student_name'], 'student_email' => $each_student_in_moodle['student_email'], 'logged_in'=>$each_student_in_moodle['last_login'] ,'loggedin_unix'=>$loggedin_unix); 

if($each_student_in_moodle['last_login']=='0000-00-00 00:00:00')
$moodle_not_logged_in[$each_student_in_moodle['student_number']] =  array ( 'student_name' => $each_student_in_moodle['student_name'], 'student_email' => $each_student_in_moodle['student_email'], 'logged_in'=>$each_student_in_moodle['last_login'],'loggedin_unix'=>$loggedin_unix ); 

else{
	
$moodle_logged_in[$each_student_in_moodle['student_number']] =  array ( 'student_name' => $each_student_in_moodle['student_name'], 'student_email' => $each_student_in_moodle['student_email'], 'logged_in'=>$loggedin_time,'loggedin_unix'=>$loggedin_unix); 
}
}
 


$student_attended= $db->query("SELECT `studid`,count(*) AS sessions_attended FROM `attendance` group by `studid`");
$students_attended_sob = $student_attended->rows;

foreach($students_attended_sob as $each_attending){
$sob_attendance_array[$each_attending['studid']] = array ( 'student_number' => $each_attending['studid'],'sessions_attended'=> $each_attending['sessions_attended']); 	
}
 
$sob_nonattending_array = array_diff_key($sob_array,$sob_attendance_array); 


//GETTING UPLOAD DATE

$dir = "students_list_temp";
$dh  = opendir($dir);
while (false !== ($filename = readdir($dh))){
  if (strpos($filename,'_moodle') !== false) {
   $files[] = $filename;
  }
}

rsort($files);
 
$time_uploaded = str_replace('_moodle','',$files[0]);
$time_uploaded = str_replace('.xls','',$files[0]);

?>


<span style="float:right;">Moodle data generated on : <?php echo date('l jS \of F Y h:i:s A', $time_uploaded);?></span></h1>




<h1>In Sob not in Moodle</h1><br><br>
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25">S.No</th>
          <th>Student Number</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Sessions attended</th>
      </tr>
      
      <?php
 
		
		$moodle_vs_sob_counter=1;
		$moodle_vs_sob = array_diff_key($sob_array,$moodle_array);
		
		foreach($moodle_vs_sob as $key => $student_detail){
		 	
			?>
        
	   <tr>
          <td width="25"><?php echo $moodle_vs_sob_counter++?></td>
          <td><?php echo $key ?></td>
          <td><?php echo $sob_array[$key]['student_firstname'] ?></td>
          <td><?php echo $sob_array[$key]['student_firstname'] ?></td>
          <td><?php echo $sob_array[$key]['student_lastname'] ?></td>
          <td><?php echo $sob_attendance_array[$key]['sessions_attended'] ?></td>
      </tr>
      <?php
		}
	 
	  
	  ?>
      
 </table><br><br>
 
 <h1>In Moodle not in SOB</h1><br><br>
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25">S.No</th>
          <th>Student Number</th>
          <th>Name</th>
          <th>Email</th>
          <th>Last login into Moodle</th>
      </tr>
      
      <?php
 
		
		$moodle_vs_sob_counter=1;
		$moodle_vs_sob = array_diff_key($moodle_array,$sob_all_students_array);
		
		foreach($moodle_vs_sob as $key => $student_detail){
	 
			?>
        
	   <tr>
          <td width="25"><?php echo $moodle_vs_sob_counter++?></td>
          <td><?php echo $key ?></td>
          <td><?php echo $moodle_array[$key]['student_name'] ?></td>
          <td><?php echo $moodle_array[$key]['student_email'] ?></td>
          <td><?php echo ($moodle_logged_in[$key]['logged_in'] !='')? $moodle_logged_in[$key]['logged_in']:'Never'; ?></td>
      </tr>
      <?php
		}
	 
	  
	  ?>
      
 </table><br><br>
 
 
 
 
 
 
<h1>Students who have not attended and not logged into moodle</h1><br><br>
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25">S.No</th>
          <th>Student Number</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          
      </tr>
      
      <?php
 
		// students who have not attended and not logged into moodle
		$list2_counter=1;
		$list2_array = array_intersect_key($moodle_not_logged_in,$sob_nonattending_array);
		
		foreach($list2_array as $key => $list2){
		 	
			?>
        
	   <tr >
          <td width="25"><?php echo $list2_counter++?></td>
          <td><?php echo $key ?></td>
          <td><?php echo $sob_array[$key]['student_firstname'] ?></td>
          <td><?php echo $sob_array[$key]['student_lastname'] ?></td>
          <td><?php echo $sob_array[$key]['student_email'] ?></td>
          
      </tr>
      <?php
		}
	 
	  
	  ?>
      
 </table><br><br>
 
 
 <h1>Students who have not attended but logged into moodle</h1>
 <br><br>
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25">S.No</th>
          <th>Student Number</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
        <th>Logged into Moodle</th>
      </tr>
      
      <?php
 
		// students who have not attended but logged into moodle (sort reverse on Moodle access)
		$list3_counter=1;
		 
		$list3_array = array_intersect_key($moodle_logged_in,$sob_nonattending_array);
		
		foreach($list3_array as $key => $list3){
		 	
			?>
        
	   <tr>
          <td width="25"><?php echo $list3_counter++?></td>
          <td><?php echo $key ?></td>
          <td><?php echo $sob_array[$key]['student_firstname'] ?></td>
          <td><?php echo $sob_array[$key]['student_lastname'] ?></td>
          <td><?php echo $sob_array[$key]['student_email'] ?></td>
          <td><?php echo $moodle_logged_in[$key]['logged_in'] ?></td>
      </tr>
      <?php
		}
	 
	  
	  ?>
      
 </table><br><br>



<h1>Students who have not logged into moodle but have attended</h1><br><br>
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25">S.No</th>
          <th>Student Number</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
        <th>Session attended</th>
      </tr>
      
      <?php
 
		// students who have not attended but logged into moodle (sort reverse on Moodle access)
		$list4_counter=1;
		$list4_array = array_intersect_key($sob_attendance_array,$moodle_not_logged_in);
		
		foreach($list4_array as $key => $list4){
		 	
			?>
        
	   <tr>
          <td width="25"><?php echo $list4_counter++?></td>
          <td><?php echo $key ?></td>
          <td><?php echo $sob_array[$key]['student_firstname'] ?></td>
          <td><?php echo $sob_array[$key]['student_lastname'] ?></td>
          <td><?php echo $sob_array[$key]['student_email'] ?></td>
           <td><?php echo $sob_attendance_array[$key]['sessions_attended'] ?></td>
      </tr>
      <?php
		}
	 
	  
	  ?>
      
 </table><br><br>
 
 
 
 
 <h1>Students with low Moodle engagement</h1>
 <br><br>
 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25">S.No</th>
          <th>Student Number</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Sessions attended</th>
          <th>Last Login</th>
          <th>Time since last login</th>
          <th>Tutor</th>
      </tr>
      
      <?php
 
		// Disengagement risk : found in all four array
		$list5_counter=1;
		$list5_array = array_intersect_key($moodle_logged_in,$sob_attendance_array);
		
		function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
		$sort_col = array();
		foreach ($arr as $key=> $row) {
		$sort_col[$key] = $row[$col];
		}
		array_multisort($sort_col, $dir, $arr);
		}
		
		array_sort_by_column($list5_array, 'loggedin_unix');
		foreach($list5_array as $key => $list5){
			
		?>
        
	   <tr>
          <td width="25"><?php echo $list5_counter++?></td>
          <td><?php echo $key ?></td>
          <td><?php echo $sob_array[$key]['student_firstname'] ?></td>
          <td><?php echo $sob_array[$key]['student_lastname'] ?></td>
          <td><?php echo $sob_array[$key]['student_email'] ?></td>
          <td><?php echo $sob_attendance_array[$key]['sessions_attended'] ?></td>
          <td><?php echo $moodle_logged_in[$key]['logged_in'] ?></td>
          <td><?php echo time_elapsed_string($moodle_logged_in[$key]['loggedin_unix'])?></td>
	  <td><?php echo $sob_array[$key]['student_tutor_name']." ".$sob_array[$key]['student_tutor_lastname']; ?></td>
      </tr>
      <?php
      if($list5_counter == 16)
			break;
		}
	 
	  
	  ?>
      
 </table><br><br>
 
</body>
</html>