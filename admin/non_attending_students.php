<?php
session_start();
include_once 'login_checker.php';
if(has_capabilities($uid, 'Attendance')==false){
	header('Location:home.php');
	exit();
}
include 'header.php';

include('weeks.php');



?>
<script>
function show_non_attendees(){
	var from = document.getElementById('week_from').value;
	var to = document.getElementById('week_to').value;
	if(parseInt(from)<=parseInt(to)){
		var url = "search_non_attending_students.php?fromweek="+from+"&toweek="+to;
		$('#non_attending_result').html('Loading... Please wait...').load(url)
	}
}
</script>
<div id="wrapper">
    <div id="wrapper_content">
        <h1 class="page_title">Non-attending Students</h1>
<br />
<form id="non_attending_form" name="non_attending_form" method="post">
<table width="350" border="0" cellspacing="0" cellpadding="10">

  <tr>
    <td>Week From</td>
    <td>
    <select id="week_from" name="week_from">
     <?php
		for($i=1;$i<=24;$i++){
			?>
            <option value="<?php echo $i;?>" <?php if($from==$i) echo 'selected="selected"';?>><?php echo $i;?></option>
            <?php
		}
		?>
    </select>
    </td>
    <td align="right">To</td>
    <td align="right"><select id="week_to" name="week_to">
     <?php
		for($i=1;$i<=24;$i++){
			?>
            <option value="<?php echo $i;?>" <?php if($to==$i) echo 'selected="selected"';?>><?php echo $i;?></option>
            <?php
		}
		?>
    </select></td>
    <td><a href="javascript:;" class="small themebutton button" onClick="show_non_attendees()"><span>Search</span></a></td>
  </tr>
</table>
</form>
<br />
      
		<div id="non_attending_result"></div>
 
        </div>
    </div>

</div>
<?php
include 'footer.php';
?>
