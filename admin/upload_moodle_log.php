<?php
session_start();
include_once('config.php');
include_once('db_class.php');
include_once('includes/excel_reader.php');
include_once('login_checker.php');

function make_it_subtractable($str){
	$str_explode = explode(' ',$str);
	$return_str = '';
	for($i=0;$i<sizeof($str_explode);$i++){
		if(is_numeric($str_explode[$i])) 
		$return_str .= ' -'.	$str_explode[$i];
		else
		$return_str .= ' '.$str_explode[$i];
			
		}
		
		return $return_str;
	}

function array_push_assoc($array, $key, $value)
{
		$array[$key] = $value;
		return $array;
}

	
	
	
		if($_FILES["student_upload"]["type"] == "application/vnd.ms-excel" || $_FILES["student_upload"]["type"] == "text/xls")
		{
			
			if ($_FILES["student_upload"]["error"] > 0)
			{
				$description="Invalid file uploaded by the user. Error : ".$_FILES["student_upload"]["error"];
				"Return code: " . $_FILES["student_upload"]["error"] . "<br />";
			}
			else
			{
				if (file_exists("students_list_temp/" . $_FILES["student_upload"]["name"]))
				{	
					unlink("students_list_temp/" . $_FILES["student_upload"]["name"]);
				}
				
				$excel_file_name = time()."_moodle".".xls";
				move_uploaded_file($_FILES["student_upload"]["tmp_name"],"students_list_temp/" .$excel_file_name);
				$description= $_FILES["student_upload"]["name"].' saved as '.$excel_file_name." and  uploaded";

				$file_path= "students_list_temp/" . $cur_file_name;
				
			}
	  }
	  else
	  {
		  $description="Invalid file uploaded by the user";
	
		  ?>
		  <script>alert('Invalid file. Please upload a file in xls format.');</script>
		  <?php
	  }

	
	$data = new Spreadsheet_Excel_Reader("students_list_temp/" . $excel_file_name, false, 'UTF-8');
	
	
	$count = $data->rowcount($sheet_index=0);
	$col_count = $data->colcount($sheet_index=0);
	
	$result_array = array();// used to store the index of  COLHEADS 	

	$template_array =array("Student Name", "Student Number", "Email", "Campus", "Country","Last Login"); 
	
	//truncate  `moodle_imported_students` to re-import
	
	$db->query("truncate `moodle_imported_students`");
	
	$trow_in_excel = $count-1;
		
		for ($i = 1; $i <= $col_count; $i++)
		{
			$result =  $data->val(1,$data->colindexes[$i]);
			
			for($j=0;$j<count($template_array);$j++)
			{
				
				if(strpos($result,$template_array[$j]) !== false)
				{
					array_push($result_array,$i);
				}
			}
		}
		$ignored_row = 0;
		$valid_count = 0;
		$updated_row = 0;
		$error_count = 0;
		
		for($excel_index=2;$excel_index<=$count;$excel_index++)
		{
			
				 
			for($templete_index=0;$templete_index<count($result_array);$templete_index++)
			{
				$c_head = $templete_index;
				$result= $data->val($excel_index,$data->colindexes[$result_array[$templete_index]]);
				
				$excel_array = array_push_assoc($excel_array, $c_head, $result);
			} 
			
			$flag = 0;
			$error = 0;
			
			
			if($excel_array[0]=='')
			{
				$ignored_row++;
			}
			else
			{	 
					$student_fullname = $db->escape($excel_array[0]);
					$student_number = $db->escape($excel_array[1]);
					$email = $db->escape($excel_array[2]);
					$campus = $db->escape($excel_array[3]);
					$country = $db->escape($excel_array[4]);
					$lastlogin_text = $db->escape($excel_array[5]);
					
					if(trim($lastlogin_text) !='Never')
					$lastlogin_time_stamp = date("Y-m-d H:i:s", strtotime(make_it_subtractable($lastlogin_text)));
					
					else{
					$lastlogin_time_stamp = '0000-00-00 00:00:00';	
					}
					
					
					$valid_count++;
					
					$db->query("INSERT INTO `moodle_imported_students` (`student_name`, `student_number`, `student_email`, `last_login`) VALUES ('$student_fullname', '$student_number', '$email', '$lastlogin_time_stamp');");
					
					
			}
			
		}
			
	
?>
<script>
window.top.window.document.getElementById('base_d').innerHTML='<div><span style="color:green">Total new records : <strong><?php echo $valid_count?></strong></span> | <span style="color:orange;">Total ignored rows : <strong><?php echo $ignored_row;?></strong></span> | Click <a href=\'javascript:print_disengagement_report()\'>here</a> to view the report</div>';
window.top.window.$('#submit_button').text('Submit');
</script>
