<?php
session_start();
include 'login_checker.php';


if(has_capabilities($uid, 'Attendance')==false){
	header('Location:home.php');
	exit();
}

extract($_GET);
if(isset($fromweek) && $fromweek!=""){
	$from = $fromweek;
}
else{
	$from = 1;
}

if(isset($toweek) && $toweek!=""){
	$to = $toweek;
}
else{
	$to = 24;
}


?>

 <table width="100%" class="content_table" border="0" cellpadding="10" cellspacing="1">
	  <tr class="table_heading">
          <th width="25" align="left">S.No</th>
          <th width="120" align="left">Student Number</th>
          <th align="left">First Name </th>
          <th align="left">Last Name </th>
          <th width="120" align="left">Email</th>
          <th width="20">Visa</th>
          <th align="left">Tutor</th>
       
       
      </tr>
<?php
$query="SELECT student_id,student_number, students.firstname as sfname,students.lastname as slname,students.visa as visa, students.email as semail ,staffs.firstname as stafffn, staffs.lastname as staffln FROM `students`,staffs WHERE `student_number` NOT IN (select distinct studid from attendance where week between $from and $to) and student_status = 0 and students.staff_id = staffs.staff_id ";



$student_obj = $db->query($query);
$student_no = $student_obj->num_rows;

if($student_no!=0){
	$students = $student_obj->rows;
	$s=0;
	foreach($students as $student){
	$s++;
	?>
    <tr <?php if($student['student_status']=="1") echo 'class="highlight_color"';?> id="student_row_<?php echo $student['student_id'];?>">
          <td align="left" valign="top"><?php echo $s;?></td>
          <td align="left" valign="top"><a href="javascript:;" title="Student Report" onclick="student_report('<?php echo $student['student_number'];?>')"><?php echo $student['student_number'];?></a></Td>
          <td valign="top" align="left"><?php echo $student['sfname'];?></td> 
          <td valign="top" align="left"><?php echo $student['slname'];?></Td>
          <td align="left" valign="top"><?php echo $student['semail'];?></Td>
	  <td valign="top" align="left"><?php if ($student['visa']) { echo "Y";} else {echo " ";} ?></Td>
          <td align="left" valign="top"><?php echo $student['stafffn']." ".$student['staffln'];?></Td>
									
   
   
    </tr>
    <?php
	}
}
else{
	?>
     <tr>
        <Td align="center" colspan="4"><br /><b>-- No results found --</b></Td>
        </tr>
    <?php
}
?>
</table>
