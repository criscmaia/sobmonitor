# SOBMONITOR #

## Table of Contents ##

[Disclaimer](#markdown-header-disclaimer)  
[Introduction](#markdown-header-introduction)  
[SOBMONITOR Overview](#markdown-header-sobmonitor-overview)  
[SOBMONITOR: features for staff](#markdown-header-sobmonitor-features-for-staff)  

##Disclaimer##
We are happy to receive bug reports and we will try our best to fix them. Please submit a bug report if you notice anything unusual. Please notice that we do not have time provide technical support. If you plan to use SOBMONITOR, you should be familiar with PHP and with the management of a MySQL database. More importantly, as mentioned in the license file:
*This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*

##Introduction##
You can think of a Student Observable Behaviour (SOB) as a very fine grained learning outcome. Something like 
*"Convert numbers between binary, decimal, octal and hexadecimal, explaining what you are doing, and carrying out simple addition in binary"*

In our first year of CS at Middlesex University we have approximately 125 SOBs in total, split into 3 levels of "difficulty": threshold, typical, excellent. Each student must show all the threshold SOBs to pass the year, even one missing threshold results in failing the year. We expect that students will also be observed on a large number of typical SOBs and some excellent. 

We employ the tool released here to keep track of all this.

##SOBMONITOR overview##

As a member of staff, you can use SOBMONITOR to:
* Record the observation of a SOB for a student of for a group of students
* Take attendance for a specific session
* Explore the records in a number of ways, including: checking attendance for a specific student or for a group of students, checking the progress of a specific student, checking the progress of the overall class, adding notes to students, etc.
This is a view of an administrator with full access to the system:
![staff_overview.png](https://bitbucket.org/repo/97XGnR/images/4044747460-staff_overview.png)

Students have access to the tool as well. When they log in, they can see the list of SOBs, check their progress with respect to expected progression dates and check their progress with respect to the rest of the class. 

## SOBMONITOR: features for staff ##

Once you have installed the tool you will see a menu bar at the top with the following items (assuming that you are logged in as a user with full access):

* Staff: here you can create new users granting them various capabilities. See below for details about LDAP and other forms of authentication.
* Students: you can choose between View/Edit and Import and Sync. Start by importing students using the Excel template file available from the web page (make sure that the upload directory is writable by the web server). When you have students loaded you can assign them a tutor (a member of staff as defined above). You have a number of other actions available: view attendance, observe SOB, student report, notes, deactivate students. Apart from the obvious options: "student report" will print an overview of the observed SOBs, attendance, and recorded notex. "Deactivate student" can be used to keep track of a student who has left in the middle of the year. Deactivated students are not plotted in the dashboard and are not part of all the other reports.