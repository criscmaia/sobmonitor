<?php

/* A simple script to generate a mail report with 
   number of observed students for SOB bands. Something like:
   0 -> 4
   1 -> 2
   [...]
   33 -> 2
   Which means: 4 students have 0 SOBs, 2 students have 1 sob, ..., 
   2 students have 33 SOBs (all of them)

   This script is normally invoked by a cron job
*/

$curdate = date("Y-m-d H:i:s");

$to = "CHANGEME@EXAMPLE.COM,CHANGEME2@EXAMPLE.COM";
$subject = "[CS Year 1] Students in SOB bands report ".$curdate;
$headers = 'From: CHANGEME@EXAMPLE.COM'. "\n".
           'Reply-To: CHANGEME@EXAMPLE.COM';
           

include_once('config.php');
include_once('db_class.php');

$db = new MySQL_Class(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

$query =  "
-- student has been observed for some sobs but some others are missing
SELECT students.student_id, students.staff_id, students.firstname as fname, students.lastname as lname, students.student_number , students.student_status, students.email, sum(sobs.level_id=1) as threshold,sum(sobs.level_id=2) as typical,sum(sobs.level_id=3) as excellent from students, sobs where (sobs.sob_id not in (select sob_observations.sob_id from sob_observations where sob_observations.student_id = students.student_id))  group by students.student_id
UNION
-- student has never been observed
SELECT students.student_id, students.staff_id, students.firstname, students.lastname, students.student_number, students.student_status, students.email, sum(sobs.level_id=1) as threshold,sum(sobs.level_id=2) as typical,sum(sobs.level_id=3) as excellent from students, sobs where students.student_id not in (select distinct sob_observations.student_id from sob_observations)  group by students.student_id 
UNION
-- there are no sobs to be observed for this student (everything done)
SELECT students.student_id, students.staff_id, students.firstname,
students.lastname, students.student_number, students.student_status,
students.email, 0 as threshold, 0 as typical, 0 as excellent from
students, sobs
where
 not exists (select sobs.sob_id from sobs where
sob_id not in (select
sob_observations.sob_id from sob_observations where
sob_observations.student_id = students.student_id) )
group by students.student_id";

$student_obj = $db->query($query);
$student_no = $student_obj->num_rows;
$studs = array();
$msg = "This message was generated on $curdate. Please contact CHANGEME if ";
$msg .="you have questions.\r\n\r\n";
if($student_no!=0){
	$students = $student_obj->rows;
	$s=0;
	foreach($students as $student){
          if ($student['student_status']!=1) {
            $s++;
            //echo "Student: ".$student['student_number'];
            //echo " N. threshold: ".$student['threshold'];
    	    //echo " Status: ".$student['student_status'];
            //echo "\n";
            $studs[] = 33-$student['threshold'];
          }
	}
        $cv = array_count_values($studs);
        for ($i = 0; $i<=33; $i++) {
          if (array_key_exists($i,$cv)) {
             $msg .= $i.",".$cv[$i]."\n";
          } else {
             $msg .= $i.",0\n";
          }
        }
        
}

mail($to,$subject,$msg,$headers);

?>

