$(document).ready(function(){

	$('#walkthrough').pagewalkthrough({

		steps:
        [
               {
                   wrapper: '',
                   margin: 0,
                   popup:
                   {
                       content: '#intro',
                       type: 'modal',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '400'
                   }        
               },
			   {
                   wrapper: '#levels_filter_area',
                   margin: '0',
                   popup:
                   {
                       content: '#levels_filter_area_help',
                       type: 'tooltip',
                       position: 'left',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '350'
                   }     
               },
			   
			   {
                   wrapper: '#topic_filter_area',
                   margin: '0',
                   popup:
                   {
                       content: '#topic_filter_area_help',
                       type: 'tooltip',
                       position: 'left',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '350'
                   }     
               },
			   			   
			 			   
			   {
                   wrapper: '#completion_filter_area',
                   margin: '0',
                   popup:
                   {
                       content: '#completion_filter_area_help',
                       type: 'tooltip',
                       position: 'left',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '350'
                   }     
               },
			  
			   
			   {
                   wrapper: '#status_filter_area',
                   margin: '0',
                   popup:
                   {
                       content: '#status_filter_area_help',
                       type: 'tooltip',
                       position: 'left',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '350'
                   }     
               },
               {
                   wrapper: '#page_contents',
                   margin: '0',
                   popup:
                   {
                       content: '#filter_results_area_help',
                       type: 'tooltip',
                       position: 'right',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '500'
                   }     
               },
               
               {
                   wrapper: '#progress_bar_display_area',
                   margin: '0',
                   popup:
                   {
                       content: '#progress_graph_area_help',
                       type: 'tooltip',
                       position: 'right',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '400'
                   }                
               },
               
               {
                   wrapper: '',
                   margin: '0',
                   popup:
                   {
                       content: '#done-walkthrough',
                       type: 'modal',
                       position: '',
                       offsetHorizontal: 0,
                       offsetVertical: 0,
                       width: '400'
                   }             
               }

        ],
        name: 'Walkthrough',
        onLoad: true,
		/*stayFocus: true,*/
        onClose: function(){
            
            return true;
        },
        onCookieLoad: function(){
            
			//alert('This callback executed when onLoad cookie is FALSE');
            return true;
        }

	});






  $('.prev-step').live('click', function(e){
	  
      $.pagewalkthrough('prev',e);
	  jQuery('html, body').animate({scrollTop: 0}, 0);
  });

  $('.next-step').live('click', function(e){
	  
      $.pagewalkthrough('next',e); 
	  jQuery('html, body').animate({scrollTop: 0}, 0);
  });

  $('.restart-step').live('click', function(e){
      $.pagewalkthrough('restart',e);
  });

  $('.close-step').live('click', function(e){
      $.pagewalkthrough('close');
  });

});