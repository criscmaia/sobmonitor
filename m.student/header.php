<?php
session_start();
include 'config.php';
include_once('db_class.php');
$currentFile = $_SERVER["PHP_SELF"];
$parts = explode('/', $currentFile);
$page = $parts[count($parts) - 1];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SOB - Student</title>
        
    <link rel="stylesheet" href="css/css-buttons.css" type="text/css" title="default" />
    <link rel="stylesheet" href="css/jquery.datepicker.css" type="text/css" title="default" />
    <link rel="stylesheet" href="css/jquery.jgrowl.css" type="text/css" title="default" />
    <link href="css/highlighted_info.css" rel="stylesheet" type="text/css" />
    <link href="css/dropdown_one.css" rel="stylesheet" type="text/css" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" />  
    <link href="css/grayout.css" rel="stylesheet" type="text/css" />  
    <script type="text/javascript" language="JavaScript" src="js/jquery.js"></script>
    <script type="text/javascript" language="JavaScript" src="js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="js/jquery.datepicker.js"></script>
	<script type="text/javascript" language="JavaScript" src="js/jquery.jgrowl.js"></script>
	<script type="text/javascript" src="../plugins/jquery.pagewalkthrough-1.1.0.js"></script>
    <script type="text/javascript" language="JavaScript" src="js/jquery.autocomplete.js"></script>
<?php
if($page=="dashboard.php")
{
	?>
    <!--<script type="text/javascript" language="JavaScript" src="js/"></script>
   <link href="css/" rel="stylesheet" type="text/css" />-->
	<?php
}


?>


</head>
<body>
<div class="site_header">

<div id="logo"><img src="images/logo-mdx.gif" height="45" /></div>
<div style="float:right;margin:20px 10px 0px 0px;color:#FFF">Logged in as <strong><?php echo $person_loggedin; ?></strong> (<?php echo $person_loggedin_email; ?>)
</div>

<div style="clear:both"></div>
</div>

<div class="site_menu">
<ul id="menu">
	<li <?php echo ($page == "home.php")? 'class="current"':'';?>><a href="home.php"><b>Dashboard</b></a></li>
    <li <?php echo ($page == "sobs.php")? 'class="current"':'';?>><a href="sobs.php"><b>SOB</b></a></li>
    <li <?php echo ($page == "report.php")? 'class="current"':'';?>><a href="report.php"><b>Reports</b></a></li>
    <?php /*?><li <?php echo ($page == "contact_tutor.php")? 'class="current"':'';?>><a href="contact_tutor.php"><b>Contact</b></a></li><?php */?>
    <li><a href="logout.php"><b>Logout</b></a></li>
    
    <li style="float:right;"><a href="#" onclick="jQuery('html, body').animate({scrollTop: 0}, 1000);$.pagewalkthrough('show', 'walkthrough');">Help</a></li>
    
</ul>

</div>