<?php
session_start();
include_once('login_checker.php');
include 'header.php';
include_once('db_class.php');
?>
<link type="text/css" rel="stylesheet" href="../plugins/css/jquery.pagewalkthrough.css" />


<!-- Standard Script Includes -->
<!--<script type="text/javascript" src="../plugins/jquery-1.7.2.min.js"></script>-->

<!--
//WALKTHROUGH DISABLED ON 13.12.2013 - since we didnthave enough text content
<script type="text/javascript" src="walkthrough_scripts/sobs_walkthrough.js"></script>-->

<script>
function sob_notes(sob_id){
	var height = $(window).height();
	var url = "sob_notes.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&sob_id='+sob_id);
}


function sob_discussion(sob_id){
	var height = $(window).height();
	var url = "discussion_sob.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&sob_id='+sob_id);
}


function insert_discussion_note(){
	var flag = 0;
	if(document.getElementById('discussion_note').value==""){
		document.getElementById('discussion_note').style.borderColor="#FF0000";
		flag=1;
	}
	else{
		document.getElementById('discussion_note').style.borderColor="";
	}
	
	
	
	if(flag==0){
		var vals = $('#reply_form').serialize();
		$.post('insert_discussion_note.php', vals, function(response){
			$('#reply_table').append(response);
			document.getElementById('discussion_note').value=""
		});
	}
}



function filter_sob_observe_list(){

	
	var flag = 0;
	var levels = "";var topics = ""; var sdate = ""; var edate = ""; var sobstatus="";var keywords_sob_id = "";
	
	var level_arr = document.getElementsByName('levels');
	for(var i=0;i<level_arr.length;i++){
		if(level_arr[i].checked==true){
			if(levels=="")
			levels = level_arr[i].value;
			else
			levels+=","+level_arr[i].value;
			
			flag=1;
			
		}
	}
	
	var topic_arr = document.getElementsByName('topics');
	for(var i=0;i<topic_arr.length;i++){
		if(topic_arr[i].checked==true){
			if(topics=="")
			topics = topic_arr[i].value;
			else
			topics+=","+topic_arr[i].value;
			
			flag=1;
			
		}
	}
	
	
	if(document.getElementById('start_date').value!=""){
		flag=1;
		sdate = document.getElementById('start_date').value;
	}
	
	if(document.getElementById('end_date').value!=""){
		flag=1;
		edate = document.getElementById('end_date').value;
	}
	
	
	var sob_status_arr = document.getElementsByName('sob_status');
	for(var i=0;i<sob_status_arr.length;i++){
		if(sob_status_arr[i].checked==true){
			if(sobstatus=="")
			sobstatus = sob_status_arr[i].value;
			else
			sobstatus+=","+sob_status_arr[i].value;
			
			flag=1;
		}
	}
	
	var keyword_sob_arr = document.getElementsByName('search_sob');
	for(var i=0;i<keyword_sob_arr.length;i++){
			if(keywords_sob_id=="")
			keywords_sob_id = keyword_sob_arr[i].value;
			else
			keywords_sob_id+=","+keyword_sob_arr[i].value;
			
			flag=1;
	}
	
	if(flag==1){
		$('#page_contents').html('Please wait... Loading...').load('list_observation.php?levels='+levels+'&topics='+topics+'&from_date='+sdate+'&to_date='+edate+'&sob_status='+sobstatus+'&keywords_sob_id='+keywords_sob_id);
	}
	else{
		$('#page_contents').html('Please wait... Loading...').load('list_observation.php');
	}
	
	
}
function search_keywords(input,event)
{

	var keyCode=event.which? event.which : event.keyCode;

	if(parseInt(keyCode)==13)
	{
		var val = input.value;var non_encoded_val = input.value;
		input.value="";
		
		val = encodeURIComponent(val);
		if(val!=""){
			
			$('#hidden_div').load('get_keyword_sob_id.php','keyword='+val,function(response){
					var tag_id = $(response).find('#search_keyword_id').val();
					var sob_ids = $(response).find('#key_sob_id').val();
					var html = '<div class="keyword_tags" id="tag_id_'+tag_id+'">'+non_encoded_val+' <input type="hidden" id="search_sob_'+tag_id+'" name="search_sob" value="'+sob_ids+'" > <a href="javascript:;" onclick="remove_search_keyword('+tag_id+')">x</a></div>'
					$('#sob_keywords_tags').append(html);
				
			});
		}
	}
}

function remove_search_keyword(tag_id){
	$('#tag_id_'+tag_id).remove();	
}

$(document).ready(function(){
	$('.datepicker').datepicker();
	$("#keywords").autocomplete("keyword_suggestions.php");
});

function update_notes(sob_id,student_id){
	var notes = $('#sob_notes').val();
	
	if(document.getElementById('public').checked==true){
		var pub = 1;	
	}
	else{
		var pub = 0;
	}
	if(notes!=""){
		notes = encodeURIComponent(notes);
		$('#hidden_div').load('update_notes.php?student_id='+student_id+'&sob_id='+sob_id+'&notes='+notes+'&public='+pub,function(response){
			cover_close();
			$('#notes_btn_'+sob_id).html(response);
			$.jGrowl("Comments updated successfully");
		});
	}
}

</script>
<div id="wrapper">
    <div id="wrapper_content">
        
      <h1 class="page_title">View SOBs</h1>


        <div id="page_contents">
            <?php
            include 'list_observation.php';
            ?>
      </div>
        <div id="right_info">
			<?php
            include 'filter_sob.php';
            ?>
        </div>
      
  </div>
</div>
<!-- walkthrough -->
<div id="walkthrough">
	<div id="intro" style="display:none;">
		<p class="tooltipTitle">VIEW SOBS</p>
        <p>This walkthrough will take you through various options in this page, click NEXT to proceed  </p>
        <br>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
	
    <div id="levels_filter_area_help" style="display:none;">
		<p class="tooltipTitle">Here you can select the level</p>
        <p>Each SOB has a level. Remember you need to pass all Threshold SOBs to pass the year!</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
    
    <div id="topic_filter_area_help" style="display:none;">
		<p class="tooltipTitle">Here you can select a topic</p>
  		<p>Each SOB is related to a topic, check this if you want to filter a specific topic.</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div> 
    
    <div id="completion_filter_area_help" style="display:none;">
        <p class="tooltipTitle">Here you can select a date</p>
        <p>You can filter SOBs based on their expected observation date. Then, press <b>Apply Filter</b> to see the results on the left</p>
        <br />
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div> 
    
    <div id="status_filter_area_help" style="display:none;">
		<p class="tooltipTitle">Here you can select the status</p>
  		<p>You can also select Observer / Unobserved SOBs</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>  

     
    <div id="filter_results_area_help" style="display:none;">
     <p class="tooltipTitle">Here you will see the results of your filter.</p>
     <p>Overdue SOBs are displayed with a red square on the left, expected SOBs with an
        orange square, and everything else in grey.
         Scroll down to see all SOBs, and if there is a not you will see a number greater
          than zero in the green button. Click on it to read the note.</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
	
	<div id="progress_graph_area_help" style="display:none;">
		<p class="tooltipTitle">This is an overview of your progress</p>
   	        <p>This bar gives you an overview of your progress for the 
	     specific level, </p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
        <a href="javascript:;" class="next-step" style="float:right;">Next Step</a>
	</div>
	
	<div id="done-walkthrough" style="display:none;">
		<p class="tooltipTitle">That's it!</p>
        <p>Feel free to contact us if you have additional queries.</p>
        <p>Click 'close' at the top right corner to close this walkthrough</p>
        <br>
        <a href="javascript:;" class="prev-step" style="float:left;">Prev Step</a>
	</div>
</div>
<?php
include 'footer.php';
?>