<?php
session_start();
include_once('login_checker.php');


$stud_obj = $db->query("SELECT * FROM `students` WHERE `student_id` = '$uid'");
$stud_no = $stud_obj->num_rows;
extract($_GET);
if($stud_no==0){
	
	?>
		<table width="600" class="content_table" border="0" cellpadding="10" cellspacing="1">
		 <tr>
			<Td align="center"><b style="color:#F00">Invalid Student ID</b></Td>
		  </tr>
		</table>
    <?php
	
}
else{
	$stud_details = $stud_obj->row;
	$student_id = $stud_details['student_id'];
	
	$today = date('Y-m-d');
	$week = date('Y-m-d',strtotime("+7 days"));
	?>


<br />


			
	<?php
	$sql = "SELECT s.sob_id, s.sob, s.level_id, l.level, s.topic_id, t.topic, s.expected_completion_date, s.url FROM `sobs` s, `levels` l, `topics` t WHERE s.level_id = l.level_id and s.topic_id = t.topic_id";
	
	if($levels!=""){
		$sql.=" AND l.level_id IN ($levels)";
	}
	
	if($topics!=""){
		$sql.=" AND t.topic_id IN ($topics)";
	}
	
	if($from_date!=""){
		$from_date = date_mysql($from_date);
		$sql.=" AND s.expected_completion_date >= '$from_date' ";
	}
	
	if($to_date!=""){
		$to_date = date_mysql($to_date);
		$sql.=" AND s.expected_completion_date <= '$to_date' ";
	}
	
	if($sob_status!="" && $sob_status!="1,2"){
		if($sob_status=="1"){
			$sql.=" AND s.sob_id IN (SELECT sob_id FROM `sob_observations` WHERE `student_id` = '$student_id') ";
		}
		else if($sob_status=="2"){
			$sql.=" AND s.sob_id NOT IN (SELECT sob_id FROM `sob_observations` WHERE `student_id` = '$student_id')";
		}
	}
	
	if($sob_ids!=""){
		$sql.=" AND s.sob_id IN ($sob_ids) ";
	}
	
	
	$sql.=' ORDER BY s.level_id, s.topic_id ';
	
	$sobs_obj = $db->query($sql);
	$sobs_no = $sobs_obj->num_rows;
	
	$divnames=1;
	
	$total = 0;
	$expected = 0;
	$observed = 0;
	
	if($sobs_no!=0){
		$sobs = $sobs_obj->rows;
		
		$topic_name = "";
		$level_name = "";
		
		$level_arr = array();
		
		?>
        
        <table width="600" cellpadding="0" cellspacing="0">
	
				
				<tr>
                  <td align="left"><strong>ECD</strong> - <strong>E</strong>xpected <strong>C</strong>ompletion <strong>D</strong>ate</td>
				  <td align="right">
                  <ul class="observe_legend">
                  	<li class="color_box sob_expired">&nbsp;</li>
                    <li>Overdue</li>
                    <li class="color_box sob_observed">&nbsp;</li>
                    <li>Observed</li>
                    <li class="color_box sob_expire_today">&nbsp;</li>
                    <li>Active</li>
                  </ul>
                  </td>
				</tr>
	
			</table>
        
		<table width="600" border="0" cellpadding="10" cellspacing="1">
		<?php
		$s=0;
		foreach($sobs as $sob){
		
		$sob_id = $sob['sob_id'];
		
		if($level_name!=$sob['level']){
	
		
			
			
			?>
            
           
            
			<tr>
				<td class="level_name" colspan="2"><?php echo $sob['level'];?></td>
			</tr>
             
			<?php
			$total = 0;
	$expected = 0;
	$observed = 0;
			$level_name=$sob['level'];
		}
		
		if($topic_name!=$sob['topic']){
			?>
			<tr>
				<td class="topic_name" colspan="2"><?php echo $sob['topic'];?></td>
			</tr>
			<?php
			$topic_name=$sob['topic'];
			$s=0;
		}
		
		$s++;
		$total++;
		
		if($sob['expected_completion_date']<$today){
			$expected++;
		}
		
		$obs_obj = $db->query("SELECT * FROM `sob_observations` WHERE `student_id` = '$student_id' AND `sob_id` = '$sob_id'");
		$obs_no = $obs_obj->num_rows;
		$obs_row = $obs_obj->row;
		
		$notes_obj = $db->query("SELECT * FROM `sob_notes` WHERE `student_id` = '$student_id' AND `sob_id` = '$sob_id'");
		$notes_no = $notes_obj->num_rows;
		?>
		<tr class="sob_highlight">
			  <td align="left" <?php if($sob['expected_completion_date']<$today && $obs_no==0)  echo 'class="sob_expired"';  elseif($sob['expected_start_date']<$today && $sob['expected_completion_date']>$today && $obs_no==0) echo 'class="sob_expire_today"'; else if($obs_row['observed_on']!='0000-00-00' && $obs_row['observed_on']!='') echo 'class="sob_observed"'; ?> width="10"><?php echo $sob['sob_id'];?></td>
			  <td align="left">
			  <?php echo $sob['sob'];?>
              <?php
			  if($sob['url']!=""){
				  echo '<br /><strong>URL : </strong>';
				  $urls = explode(' ',$sob['url']);
				  foreach($urls as $url){
					  if($url!="" && $url!=" "){
					  	echo '<a href="'.$url.'" target="_blank">'.$url.'</a> &nbsp; &nbsp; ';
					  }
				  }
			  }
			  ?>
              </Td>
		</tr>
			<tr>      
			  <td colspan="2">
			  <div style="float:left;"><strong>ECD:</strong> <?php echo date_ft($sob['expected_completion_date']); if($sob['expected_completion_date']<$week && $obs_no==0 && $sob['expected_completion_date']>=$today) { echo ' <b>'. DayDifference($today, $sob['expected_completion_date']) . ' day(s) left</b>'; }?></div> 
              
              <div style="float:right;"><a class="small green button" id="notes_btn_<?php echo $sob_id;?>" onClick="sob_notes('<?php echo $sob_id;?>')" href="javascript:;">Comments (<?php echo $notes_no;?>)</a>&nbsp;&nbsp;<a class="small green button" onClick="sob_discussion('<?php echo $sob_id;?>')" href="javascript:;">Notes</a></div>
              <?php
			  if($obs_no==0){
					echo '<div style="float:right;padding-top:5px;">Not yet observed &nbsp;&nbsp;</div>';
				}
				else{
				$staffid = $obs_row['observed_by'];
				$obs_id = $obs_row['observation_id'];
				
					if($staffid!="0"){
						$teacher_obj = $db->query("SELECT * FROM `staffs` WHERE `staff_id` = '$staffid'");
						$teacher_details = $teacher_obj->row;
						$teacher_name = $teacher_details['firstname'] . " " . $teacher_details['lastname'];
						$observed_by = ' by ' . $teacher_name;
						
					}
					else{
						$observed_by = '';
					}
				$observed++;
				
					
				
					if($obs_row['observed_on']!='0000-00-00'){
					 echo '<div style="float:right;padding-top:5px;"><strong>Observed on</strong> : ' . date_ft($obs_row['observed_on']). $observed_by . '  &nbsp;&nbsp;</div>';
					}
					else{
					
					}
				
				}
				?>
			  			
              
			  </td>
			</tr>
		<?php
		}
		?>
        
	</table>


		<?php
	}
	else{
		?>
		<table width="600" class="content_table" border="0" cellpadding="10" cellspacing="1" align="left">
		 <tr>
			<Td align="center"><b>-- No results found --</b></Td>
		  </tr>
		</table>
		<?php
	}
}
?>

