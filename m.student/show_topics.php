<?php
session_start();
include_once('login_checker.php');
?>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="dues_table">
    <tr class="dues_header_tr">
        <th width="30">SNo</th>
        <th>Subject</th>
        <th width="80">Replies</th>
        <th width="100">Datetime</th>
        <th width="100">Action</th>
    </tr>
    <?php
    $topics_obj = $db->query("SELECT * FROM `contact_thread` WHERE `student_id` = '$uid'");
	if($topics_obj->num_rows>0){
		$topics = $topics_obj->rows;
		$r=0;
		foreach($topics as $topic){
			$r++;
			$thread_id = $topic['thread_id'];
			$thread_obj = $db->query("SELECT * FROM `contact_thread_replies` WHERE `thread_id` = '$thread_id'");
			$reply_no = $thread_obj->num_rows; 
			?>
			<tr>
				<td><?php echo $r;?></td>
				<td><?php echo $topic['thread_subject'];?></td>
				<td><?php echo $reply_no;?></td>
				<td><?php echo date('d.m.Y',strtotime($topic['thread_datetime']));?></td>
                <td><a href="javascript:;" onClick="show_replies('<?php echo $topic['thread_id'];?>')"><img src="images/view.png" border="0" /></a></td>
			</tr>
			<?php
		}
	}
	else{
		echo '<tr><td colspan="4">No topic(s) found</td></tr>';
	}
    ?>
</table>