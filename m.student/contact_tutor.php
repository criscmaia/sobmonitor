<?php
session_start();
include_once('login_checker.php');
include 'header.php';
include_once('db_class.php');
?>
<style>
.tutor_detail{
	border: 1px solid #cccccc;
    clear: both;
    float: left;
    padding: 20px;
    width: auto;
}

.tutor_detail h1{
	margin:0px;
	padding:0px;
	margin-bottom:5px;	
}

.tutor_detail h3{
	margin:0px;
	padding:0px;
	margin-bottom:5px;	
}

.tutor_detail h4{
	margin:0px;
	padding:0px;	
}
</style>
<script>

function create_topic(){
	var height = $(window).height();
	var url = "create_topic.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height);
}

function insert_topic(){
	var flag = 0;
	if(document.getElementById('contact_subject').value==""){
		document.getElementById('contact_subject').style.borderColor="#FF0000";
		flag=1;
	}
	else{
		document.getElementById('contact_subject').style.borderColor="";
	}
	
	if(document.getElementById('contact_message').value==""){
		document.getElementById('contact_message').style.borderColor="#FF0000";
		flag=1;
	}
	else{
		document.getElementById('contact_message').style.borderColor="";
	}
	
	if(flag==0){
		document.contact_form.action="insert_topic.php";
		document.contact_form.submit();
		document.getElementById('submit_status').innerHTML="Submitting... Please wait...";
	}
}

function insert_reply(){
	var flag = 0;
	if(document.getElementById('thread_reply').value==""){
		document.getElementById('thread_reply').style.borderColor="#FF0000";
		flag=1;
	}
	else{
		document.getElementById('thread_reply').style.borderColor="";
	}
	
	
	
	if(flag==0){
		var vals = $('#reply_form').serialize();
		$.post('insert_reply.php', vals, function(response){
			$('#reply_table').append(response);
			document.getElementById('thread_reply').value=""
		});
	}
}
function show_topics(){
	$('#topics_holder').html('Loading...').load('show_topics.php');
}

function show_replies(thread_id){
	var height = $(window).height();
	var url = "show_replies.php";
	grayOut(true,'grayOut_center_div',800);
	$('#grayOut_center_div').load(url,'height='+height+'&thread_id='+thread_id);
}
</script>
<div id="wrapper">
    <div id="wrapper_content">
      <h1 class="page_title">Contact Tutor</h1>
        <div id="page_contents">
			<?php
			 $student_obj = $db->query("SELECT * FROM `students` WHERE `student_id` = '$uid'");
			 $student_detail = $student_obj->row;
			 if($student_detail['staff_id']!="0"){
				 $staff_id = $student_detail['staff_id'];
				 $staff_obj = $db->query("SELECT * FROM `staffs` WHERE `staff_id` = '$staff_id'");
				 $staff_detail = $staff_obj->row;
				 ?>
				 <div class="tutor_detail">
					<h1>Tutor</h1>
					<h3><?php echo $staff_detail['firstname'];?> <?php echo $staff_detail['firstname'];?></h3>
					<h4><?php echo $staff_detail['email'];?></h4>
				 </div>
				<?php
			 }
			 ?>
             <div class="clear"></div>
             <br>
			
             <a onclick="create_topic()" href="javascript:;" class="small themebutton button">Create topic</a>
             <br>
			<br>
			<br>

             <div id="topics_holder">          
             	<?php include 'show_topics.php';?>
             </div>
      	</div>
  </div>
</div>
<?php
include 'footer.php';
?>